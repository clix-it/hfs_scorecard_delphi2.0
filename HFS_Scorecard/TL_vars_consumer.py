from common_functions import *
import math
import numpy as np
from datetime import datetime, date
import dateutil.relativedelta
import logging


def mob_tl(row, current_date):
    try:
        disbursed_date = datetime.strptime(row['disbursed_dt'], '%Y-%m-%d').date()
        mob_value = math.ceil((current_date - disbursed_date).days / 30)
        return mob_value
    except Exception as e:
        return 0


def TL_vars(acc_type_id_dict, bureau, tradelines):

    sourcing_dates = bureau.sourcing_date
    disbursed_dates = [x['disbursed_dt'] for x in tradelines]

    current_date = (
            datetime.strptime(sourcing_dates, '%d-%m-%Y') - dateutil.relativedelta.relativedelta(months=1)).date()

    no_hit = check_no_hit_with_months(sourcing_dates, disbursed_dates, months=6)

    tl_vars = []

    # if tradeline and not no_hit
    if int(bureau.accounts.tradeline.num_of_tradeline) > 0 and not no_hit:
        for row in tradelines:
            row["source_date"] = sourcing_dates
            row["current_date"] = current_date

            if row['acct_type_id'] in acc_type_id_dict.keys():
                loan_type = acc_type_id_dict[row['acct_type_id']]
            else:
                loan_type = None

            tl_resp = {
                "loan_type": loan_type,
                "loan_amount": row["disbursed_amt"],
                "bank_type": row['Identification_Number'],
                "open_date": row['disbursed_dt'],
                "closed_date": row['close_dt'],
                'current_balance': row['current_bal'],
                'ownership': row['ownership_tag'],
                'imputed_emi': row['emi'],
                "mob": mob_tl(row, current_date),
                "live_account": True if row["live_account"] in [1] else False,
                # "write_off_settled": None if np.isnan(row["write_off_settled"]) else float(row["write_off_settled"]),
                # "dpd_90_plus_in_24_mnts": row["dpd_90_plus_in_24_mnts"],
                # "dpd_30_plus_in_6_mnts": row["dpd_30_plus_in_6_mnts"],
                # "dpd_x_plus_in_3_mnts": row["dpd_x_plus_in_3_mnts"],
                # "current_overdue": None if np.isnan(row["overdue_amt"]) else float(row["overdue_amt"])
            }

            tl_vars.append(tl_resp)

        return tl_vars
    else:
        return []
