# -*- coding: utf-8 -*-
"""

Consumer scorecard to validate on HFS Consumer CIR

Created on Tue Oct 20 12:27:23 2020

@author: 100763 Abhijeet.Patil@clix.capital


"""


import pandas as pd
import os
import numpy as np
import shutil   # for copying files
from functools import reduce    # for merging array of dataframes
from datetime import datetime
# modeling
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import roc_auc_score, roc_curve
# charts
# from matplotlib import pyplot as plt
# jupyter utils
# from IPython.display import clear_output
import pickle   # for models saving

# for definition refer to excel with output data
final_model_columns = ['AmtOverdue', 'os_to_loan_amount', 'unsec_AmtOverdue', 'ratio_num_trdlns_0plus_in_3m', 
                       'bureau_vintage', 'max_loan_amount', 'last_max_loan_amount', 'bl_bureau_vintage', 
                       'unsec_perc_loan_amt', 'last_max_unsec_loan_amount', 'last_max_bl_loan_amount', 
                       '0plus_in_6m_CurrBalance', '0plus_in_6m_os_to_loan_amount', 'unsec_perc_os_amt', 
                       'num_trdlns_30plus_in_3m', '30plus_in_6m_os_to_loan_amount', 'num_trdlns_0plus_in_3m', 
                       'nbr_enq_bl_1m']

# final_model_input_data = ### Parse data created from Consumer Bureau out

# ================================================================================================================
# Run models


def bureau_model(user_type, data):
    if user_type == "INDIVIDUAL":
        model_file = "/opt/python/model/final_Consumer_Bureau_model.sav"
        model = pickle.load(open(model_file, 'rb'))
        data_dict = data
        # dict to dataframe conversion
        data = pd.DataFrame([data])

        try:
            data['prediction_score'] = model.predict_proba(data[final_model_columns])[:, 1]
    
            data.head()
        except Exception as e:
            data['prediction_score'] = 1

        # get segment basis development sample thresholds
        def get_segment(prob_score):
            if prob_score <= 0.239791088:
                return 1
            if prob_score <= 0.320493341:
                return 2
            if prob_score <= 0.407382921:
                return 3
            if prob_score <= 0.479850147:
                return 4
            return 5                        # A5 - Higher Risk
    
        # data['risk_segment'] = data['prediction_score'].apply(get_segment)
        # data.to_excel("HFS_Predictions.xlsx", index=False)
        return int(data['prediction_score'].apply(get_segment).values[0])

    elif user_type == "COMPANY":
        if data is not None and type(data) is dict:
            return data["commercial_cb_score_seg"]
        else:
            return "Not Scored"
    else:
        return {"error": "USER TYPE UNKNOWN"}


def banking_model(banking_dict, demographic_dict):

    # =======================================================================================

    final_feature_list = ['Average Bank Balance', 'Credits', 'current_Nos. of Credit', 'current_Nos. of Debit',
                          'ccod_Nos. of Credit', 'ccod_Nos. of Debit', 'ccod_Debit Amount', 'Average Bank Balance max',
                          'Average Bank Balance min', 'Inward_Bounce', 'Avg_CC_OD_Util', 'Office_Owned', 'Office_Stability',
                          'Resi_Stability', 'Resi_Owned', 'AGE2', 'AGE1', 'Industry_Margin', 'Tenor', 'Shareholding2',
                          'Shareholding1', 'Business_Type_manufactur', 'Business_Type_trade', 'Branch_banglore',
                          'Branch_delhi', 'Branch_mumbai', 'Constitution/Relationship2_partner',
                          'Constitution/Relationship2_proprietor', 'Existing_Annual_obligations'
                          ]

    banking_model_data = {key: banking_dict.get(key) for key in final_feature_list if key in banking_dict}
    demographic_model_data = {key: demographic_dict.get(key) for key in final_feature_list if key in demographic_dict}

    # banking_out_perfios = Parse_Perfios_Out
    banking_out_perfios = pd.DataFrame({**banking_model_data, **demographic_model_data}, index=[0])

    # load the models from disk
    filename = '/opt/python/model/final_Banking_model.sav'

    loaded_model = pickle.load(open(filename, 'rb'))

    banking_out_perfios['Pred_Prob'] = \
        loaded_model.predict_proba(banking_out_perfios[final_feature_list].values)[:, 1]

    try:
        prob_score = float(banking_out_perfios["Pred_Prob"].values[0])
    except Exception as e:
        error = e
        prob_score = 1

    if prob_score <= 0.09656961:
        score_band = "BN1"
    elif 0.09656961 < prob_score <= 0.14980566:
        score_band = "BN2"
    elif 0.14980566 < prob_score <= 0.179781029:
        score_band = "BN3"
    elif 0.179781029 < prob_score <= 0.210691688:
        score_band = "BN4"
    elif 0.210691688 < prob_score:
        score_band = "BN5"
    else:
        score_band = "BN5"

    return score_band
