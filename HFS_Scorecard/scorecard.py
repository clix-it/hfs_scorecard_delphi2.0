import os
import json
import operator
import psycopg2
import logging
import boto3
import pandas as pd
import pkg_resources
import requests
from getBureau import *
# from commercial_parser import *
from functions.response import *
from common_functions import *
from functions.conf_functions import *
from HFS.hfs_consumer import *
from HFS.hfs_commercial import *
from HFS.hfs_banking import *
from HFS.hfs_qde import *
from models.HFS_Model_Deployment_Consumer_Scorecard import *
import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

logger = logging.getLogger()
logger.setLevel(logging.INFO)

if os.environ["is_sentry"].lower() in ['true']:
    sentry_sdk.init(dsn=os.environ["sentry_dsn"], integrations=[AwsLambdaIntegration()], environment=os.environ["env"])


negative_area_pincodes = pd.read_csv(pkg_resources.resource_filename(__name__, 'NEGATIVE_AREA_PINCODE.csv'))
negative_area_pincodes["negative_area"] = negative_area_pincodes["negative_area"].str.upper()

city_category_mapping = pd.read_csv(pkg_resources.resource_filename(__name__, 'City_Category_Mapping.csv'))
city_category_mapping["City"] = city_category_mapping["City"].str.upper()


def get_decisioning_data(user_type, applicant):
    try:
        xml = applicant["bureauData"]["bureau"]
    except:
        try:
            xml = applicant["bureauData"]["bureauXML"]
        except:
            xml = None

    if xml not in [None, ""]:
        if user_type == "INDIVIDUAL":
            consumer_xml_data = Parser("MB", xml).bureau
            logger.info("Parser Run Successfully")
            applicant_bureau_data = hfs_consumer(consumer_xml_data)
        elif user_type == "COMPANY":
            commercial_xml_data = Commercial(xml)
            applicant_bureau_data = hfs_commercial(commercial_xml_data)
        else:
            applicant_bureau_data = None
    else:
        applicant_bureau_data = None

    return applicant_bureau_data


def get_scorecard(event, context):
    logger.info(f"Loading {os.environ['env']} HFS_Scorecard")
    warnings = {}

    try:
        request_url = event["report"]["additionalDetails"]["requestURL"]
        event = requests.get(url=request_url).json()
        print(event["applicationId"])
    except Exception as e:
        return {"status": "false", "error": f"{e}"}

    # event = json.load(open('testing_request.json'))

    try:
        applicationStage = event["report"]["stage"]["applicationStage"].lower()
    except:
        applicationStage = None

    if applicationStage is None:
        warnings["applicationStage"] = 'applicationStage is not present in json'
        return warning_resp_json(event, os.environ['env'], warnings)

    bureau_score = None
    pol_bureau_score = None
    app_bureau_data = None
    co_app_bureau_data = None
    applicant_data = None
    co_applicant_data = None
    app_score_band = None
    is_co_applicant = False
    is_applicant_ntc = None
    co_app_score_band = None
    dedupe_list = []
    qualification_list = []
    app_score_band_list = []
    co_app_score_band_list = []
    is_co_applicant_ntc_list = []
    co_applicant_age_list = []
    co_applicant_data_list = []
    co_app_bureau_score_list = []
    co_app_bureau_decision_status_list = []
    is_eligible_for_bank_demographic_list = []
    final_co_app_bureau_data = {"rejection_reason": [], "user_type": None}
    doctor_list = ["mbbs", "specialist(md/ms)", "mbbs+diploma"]
    non_doctor_list = ["homeopathy/unani/ayurvedic", "chinesedoctor", "others", "other"]
    rejected_doctor_list = ["dentist(bds/mds)", "russiandoctor"]
    try:
        applicants = event['report']['applicant']
    except:
        applicants = event['report']['applicants']

    loan_details = event['report']["loanDetails"]
    collaterals = event["report"]["collaterals"]

    if len(applicants) > 0:
        for applicant in applicants:
            try:
                qualification_list.append(str(applicant.get('qde').get("qualification")).lower().replace(" ", ""))
            except:
                qualification_list.append(None)

            try:
                dedupe_list.append(str(applicant.get('qde').get("dedupeStatus")).lower().replace(" ", ""))
            except:
                dedupe_list.append(False)

            if str(applicant['userRole']).upper() == 'APPLICANT':
                applicant_data = applicant
                user_type = applicant['userType']
                app_bureau_data = get_decisioning_data(user_type, applicant_data)
                if app_bureau_data is not None:
                    app_bureau_data["user_type"] = user_type
                    app_bureau_model_band = bureau_model(user_type, app_bureau_data)
                    app_score_band_list.append(app_bureau_model_band)
                    is_applicant_ntc = bool(app_bureau_data["no_hit"])
                    if app_bureau_data["no_hit"] is False and app_bureau_model_band in [1, 2, 3, 4]:
                        is_eligible_for_bank_demographic_list.append(True)
                    else:
                        is_eligible_for_bank_demographic_list.append(False)

            elif str(applicant['userRole']).upper() in ['CO-APPLICANT', 'GUARANTOR', 'PROMOTER']:
                co_applicant_data = applicant
                is_co_applicant = True
                user_type = applicant['userType']
                co_app_bureau_data = get_decisioning_data(user_type, co_applicant_data)

                if co_app_bureau_data is not None:
                    co_app_bureau_score_list.append(co_app_bureau_data["bureau_score"])
                    co_app_bureau_data["user_type"] = user_type
                    co_app_bureau_model_band = bureau_model(user_type, co_app_bureau_data)
                    if co_app_bureau_data["no_hit"] is False:
                        co_app_bureau_data["model_band"] = co_app_bureau_model_band
                        co_app_bureau_decision_status_list.append(co_app_bureau_data["pol_decision_status"])
                        final_co_app_bureau_data["rejection_reason"] += co_app_bureau_data["rejection_reason"]
                    else:
                        co_app_bureau_data["model_band"] = "NTC"

                    is_co_applicant_ntc_list.append(bool(co_app_bureau_data["no_hit"]))
                    if co_app_bureau_data["no_hit"] is False and co_app_bureau_model_band in [1, 2, 3, 4]:
                        is_eligible_for_bank_demographic_list.append(True)
                    else:
                        is_eligible_for_bank_demographic_list.append(False)

                    co_applicant_data_list.append(co_app_bureau_data)
                    final_co_app_bureau_data["user_type"] = user_type
            else:
                return {"error": "unknown userRole"}

        if any(qualification in doctor_list for qualification in qualification_list):
            qualification_type = 'Doctor Case'
        elif any(qualification in non_doctor_list for qualification in qualification_list):
            qualification_type = 'Non Doctor Case'
        else:
            qualification_type = 'Reject Doctor Case'

        if all(status == 'new' for status in dedupe_list):
            dedupe_status = 'Approved'
        else:
            dedupe_status = 'RTC'

        app_score_band = max(app_score_band_list, default=5)
        co_app_score_band = max([i["model_band"] for i in co_applicant_data_list if not i["model_band"] == "NTC"], default=5)
        is_co_applicant_ntc = all(is_co_applicant_ntc_list) if len(is_co_applicant_ntc_list) > 0 else False

        is_eligible_for_bank_demographic = False
        is_eligible_for_bureau_bank_demo = False
        if any(is_eligible_for_bank_demographic_list):
            is_eligible_for_bank_demographic = True
        elif all(str(i["model_band"]) == 'NTC' for i in co_applicant_data_list) and is_applicant_ntc:
            is_eligible_for_bank_demographic = True
        elif is_applicant_ntc and is_co_applicant is False:
            is_eligible_for_bank_demographic = True

        if (is_applicant_ntc is False and is_co_applicant is False) or (is_applicant_ntc is False and is_co_applicant_ntc is False and is_co_applicant is True):
            is_eligible_for_bureau_bank_demo = True
            is_eligible_for_bank_demographic = False

        if app_bureau_data is None:
            app_bureau_data = {"user_type": None, "pol_decision_status": "Decline", "rejection_reason": ["Applicant bureau data is blank"]}

        try:
            co_app_bureau_score_list.append(app_bureau_data["bureau_score"])
        except:
            pass

        try:
            bureau_score_list = [int(score) if int(score) > 300 else 0 for score in co_app_bureau_score_list]
            bureau_score = sum(bureau_score_list) / int(len(bureau_score_list) - bureau_score_list.count(0))
        except:
            bureau_score = 0

        pol_bureau_score = True if bureau_score >= 700 else False

        if is_co_applicant:
            if not len(co_app_bureau_decision_status_list) == 0 and all("Accept" == status for status in co_app_bureau_decision_status_list):
                final_co_app_bureau_data["pol_decision_status"] = "Accept"
            elif any("Decline" == status for status in co_app_bureau_decision_status_list):
                final_co_app_bureau_data["pol_decision_status"] = "Decline"
            elif any("Refer" == status for status in co_app_bureau_decision_status_list):
                final_co_app_bureau_data["pol_decision_status"] = "Refer"
            else:
                final_co_app_bureau_data["pol_decision_status"] = "Decline"
        else:
            final_co_app_bureau_data["pol_decision_status"] = "Accept"

    else:
        return {"error": "Applicant data not found"}

    app_co_app_qde_data, demographic_score_band, asset_type_cat, total_asset_value, \
        max_perc_collateral, asset_condition, asset_type = hfs_qde_data(applicants, loan_details,
                                                                        collaterals, negative_area_pincodes,
                                                                        city_category_mapping, qualification_type,
                                                                        qualification_list)

    try:
        bank_details = applicant_data['bankDetails']
        try:
            is_bank_details = bool(bank_details[0]['analysisData'])
        except:
            is_bank_details = bool(bank_details[0]['summaryInfo'])
    except Exception as error:
        bank_details = None
        is_bank_details = False

    try:
        pol_doctor_startup = app_co_app_qde_data.get("pol_business_vintage", "FAIL")
    except:
        pol_doctor_startup = "FAIL"

    if bank_details is not None and is_bank_details:
        app_banking_data = get_banking(bank_details, applicants, applicant_data, pol_doctor_startup)
    else:
        app_banking_data = None

    if app_banking_data is not None and app_co_app_qde_data is not None:
        banking_demographic_score_band = banking_model(app_banking_data, app_co_app_qde_data)
    else:
        banking_demographic_score_band = None

    final_response = get_final_decision(app_bureau_data, final_co_app_bureau_data, app_banking_data,
                                        app_co_app_qde_data, app_score_band, co_app_score_band,
                                        banking_demographic_score_band, demographic_score_band, is_applicant_ntc,
                                        is_co_applicant_ntc, is_co_applicant, is_eligible_for_bank_demographic,
                                        is_eligible_for_bureau_bank_demo)

    final_eligibility, final_abb_emi, abb_to_emi, ltv, gross_ltv, ltv_asset, pol_eligibility_decision, \
        eligibility_reject_reasons, final_emi_loan_amount, \
        initial_abb_loan_amount, loan_amount_asset_ltv, common_var_dict = get_final_eligibility(final_response, loan_details,
                                                                                   app_banking_data, asset_type_cat,
                                                                                   total_asset_value,
                                                                                   asset_condition, asset_type)

    logger.info(final_response)

    return response_json(event, final_response, pol_eligibility_decision, eligibility_reject_reasons, app_score_band,
                         co_app_score_band, app_bureau_data,
                         co_applicant_data_list, app_banking_data, app_co_app_qde_data,
                         banking_demographic_score_band, demographic_score_band, final_eligibility,
                         final_emi_loan_amount, initial_abb_loan_amount, loan_amount_asset_ltv, final_abb_emi, abb_to_emi, ltv,
                         asset_type_cat, gross_ltv, ltv_asset, common_var_dict, max_perc_collateral, bureau_score, pol_bureau_score,
                         is_applicant_ntc, is_co_applicant_ntc, is_co_applicant,
                         is_eligible_for_bank_demographic, is_eligible_for_bureau_bank_demo, dedupe_status, applicationStage)
