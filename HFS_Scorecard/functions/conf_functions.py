import dateutil.relativedelta
import numpy as np
import math
from functools import reduce
from dateutil import parser
from datetime import datetime, date
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def del_gold_loan_agri_loans_ownership(type_of_xml, tradelines):
    try:
        for row in tradelines:
            if type_of_xml == "MB":
                if row['acct_type_id'] in ["A09", "A25", "A29", "A43", "A63"] or row['ownership_ind'].lower() not in ['individual', 'joint']:
                    tradelines.remove(row)

            elif type_of_xml == "CIBIL":
                if row['acct_type_name'] in ["Gold Loan"]:
                    tradelines.remove(row)

        return tradelines
    except:
        return tradelines


def overdue_fun_hfs(row):
    try:
        if not math.isnan(row['overdue_amt']) and row['overdue_amt'] not in [None] and row["acct_type_id"] not in ['A53', 'A36', 'A34']:
            try:
                overdue_amt = int(row['overdue_amt'])
            except:
                overdue_amt = int(str(row['overdue_amt']).replace(",", ""))
        else:
            overdue_amt = 0
    except:
        overdue_amt = 0

    return overdue_amt


def unsec_amt_overdue_hfs(row):
    try:
        if not math.isnan(row['overdue_amt']) and row['overdue_amt'] not in [None] and row["acct_type_id"] in ['A52', 'A51', 'A05', 'A61', 'A12', 'A54', 'A09', 'A14', 'A55', 'A43', 'A56', 'A40', 'A38']:
            try:
                overdue_amt = int(row['overdue_amt'])
            except:
                overdue_amt = int(str(row['overdue_amt']).replace(",", ""))
        else:
            overdue_amt = 0
    except:
        overdue_amt = 0

    return overdue_amt


def score_dpd1(row, p_dpd, asset, type_of_xml, dpd):
    dpd_new = p_dpd
    asset = str(asset).upper()

    if type_of_xml == "MB":
        if row['suit_filed_wilful_default'] not in [None, np.nan] and int(row['suit_filed_wilful_default']) in range(2, 5) and row["acct_type_id"] not in ["A15", "A16", "A17"] :
            dpd_new = 180
        elif row['write_off_settled'] not in [None, np.nan, "nan"] and int(row['write_off_settled']) in range(5, 16) and row["acct_type_id"] not in ["A15", "A16", "A17"] :
            dpd_new = 180
        elif row['current_bal'] is not None and int(row['current_bal']) <= 5000:
            dpd_new = 0
        elif bool({asset} & {'LOS', 'DBT', 'DOUBTFUL-1', 'DOUBTFUL-2', 'DOUBTFUL-3'}) or p_dpd >= 180:
            dpd_new = 180
        elif bool({asset} & {'SUB', 'SUB-STD', 'DBT', 'DOUBTFUL', 'LSS', 'LOSS', 'NPA'}) or p_dpd >= 90:
            dpd_new = 90
        elif bool({asset} & {'SMA', 'SMA-2', 'SPECIAL MENTION ACCOUNT'}) or p_dpd >= 60:
            dpd_new = 60
        elif bool({asset} & {'SMA-1'}) or p_dpd >= 30:
            dpd_new = 30
        elif bool({asset} & {'SMA-0'}) or p_dpd > 0:
            dpd_new = 29
        elif p_dpd <= 0:
            dpd_new = 0
        else:
            dpd_new = p_dpd

    elif type_of_xml == "CIBIL":
        if (row['suit_filed_status'] in ['Suit Filed Case', 'Suit Filed'] or
            str(row['wilful_defaulter']) in ['Wilful Defaulter'] or
            str(row['written_off']) in ['Written Off'] or
            str(row["restructure_status"]) in ["Restructure Case"] or
            str(row["co_status"]) in ["CO Case"]) and \
                row["acct_type_name"] not in ["Corporate credit card", "Loan extended through credit cards"]:
            dpd_new = 180
        elif row['current_bal'] is not None and int(row['current_bal']) <= 5000:
            dpd_new = 0
        elif bool({asset} & {"Sub-standard", "Doubtful", "Loss", "999", "above Days Past Due",
                             "999 or above Days Past Due", "DOUBTFUL-1", "DOUBTFUL-2", "DOUBTFUL-3", "Non Performing Assets",
                             "ARC Loans"}) or p_dpd >= 999:
            dpd_new = 999
        elif bool({asset} & {'LOS', 'DBT'}) or p_dpd >= 180:
            dpd_new = 180
        elif bool({asset} & {'SUB', 'SUB-STD', 'DBT', 'Doubtful', 'LSS', 'LOSS', 'NPA'}) or p_dpd >= 90:
            dpd_new = 90
        elif bool({asset} & {'SMA', 'SMA-2', 'Special Mention Account'}) or p_dpd >= 60:
            dpd_new = 60
        elif bool({asset} & {'SMA-1'}) or p_dpd >= 30:
            dpd_new = 30
        elif bool({asset} & {'SMA-0'}) or p_dpd > 0:
            dpd_new = 29
        elif p_dpd <= 0:
            dpd_new = 0
        else:
            dpd_new = p_dpd

    return dpd_new


def pol_dpd_plus_mths_fun_hfs(row, current_date, dpd, months, type_of_xml):
    count = 0
    today = current_date
    req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
    try:
        if type_of_xml is "MB":
            for i in row['dpd_dates']:
                asset = i.split('/')[1]
                i = i.split('/')[0].split('-')
                i[0] = datetime.strptime(i[0], '%b').month
                if int(i[0]) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                    d = date(int(i[1]), int(i[0]), 28)
                else:
                    d = date(int(i[1]), int(i[0]), int(current_date.day))
                if req_date <= d <= today:
                    # asset = i[2].split('/')[1]
                    i[2] = i[2].split('/')[0]
                    if i[2] in ["xxx", "XXX"]:
                        i[2] = 0
                    if int(score_dpd1(row, int(i[2]), asset, type_of_xml, dpd)) > int(dpd):
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
        elif type_of_xml == "CIBIL":
            today = current_date
            req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
            for dpd_string, dpd_with_asset in zip(row['dpd_with_month'], row["dpd_hist"]):
                trad_dpd = int(str(dpd_string).split('/')[0])
                dpd_asset = str(dpd_with_asset).split('/')[0]
                dpd_date = parser.parse('01-' + str(dpd_string).split('/')[1])
                if req_date <= dpd_date <= today:
                    if int(score_dpd1(row, int(trad_dpd), dpd_asset, type_of_xml, dpd)) > int(dpd):
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
    except:
        return 0


def pol_dpd_eq_plus_mths_fun_hfs(row, current_date, dpd, months, type_of_xml):
    count = 0
    today = current_date
    req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
    try:
        if type_of_xml is "MB":
            for i in row['dpd_dates']:
                asset = i.split('/')[1]
                i = i.split('/')[0].split('-')
                i[0] = datetime.strptime(i[0], '%b').month
                if int(i[0]) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                    d = date(int(i[1]), int(i[0]), 28)
                else:
                    d = date(int(i[1]), int(i[0]), int(current_date.day))
                if req_date <= d <= today:
                    i[2] = i[2].split('/')[0]
                    if i[2] in ["xxx", "XXX"]:
                        i[2] = 0
                    if int(score_dpd1(row, int(i[2]), asset, type_of_xml, dpd)) >= int(dpd):
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
        elif type_of_xml == "CIBIL":
            today = current_date
            req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
            for dpd_string, dpd_with_asset in zip(row['dpd_with_month'], row["dpd_hist"]):
                trad_dpd = int(str(dpd_string).split('/')[0])
                dpd_asset = str(dpd_with_asset).split('/')[0]
                dpd_date = parser.parse('01-' + str(dpd_string).split('/')[1])
                if req_date <= dpd_date <= today:
                    if int(score_dpd1(row, int(trad_dpd), dpd_asset, type_of_xml, dpd)) >= int(dpd):
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
    except:
        return 0


def pol_0_plus_current_fun_hfs(row, current_date, dpd, months, type_of_xml):
    count = 0
    # current_date = datetime.strptime(current_date, "%Y-%m-%d").date()    # Only for Experian
    # today = date(current_date.year, current_date.month, current_date.day)
    today = current_date
    req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
    try:
        if type_of_xml is "MB":
            for i in row['dpd_dates']:
                asset = i.split('/')[1]
                i = i.split('/')[0].split('-')
                i[0] = datetime.strptime(i[0], '%b').month
                if int(i[0]) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                    d = date(int(i[1]), int(i[0]), 28)
                else:
                    d = date(int(i[1]), int(i[0]), int(current_date.day))
                if req_date <= d <= today:
                    # asset = i[2].split('/')[1]
                    i[2] = i[2].split('/')[0]
                    if i[2] in ["xxx", "XXX"]:
                        i[2] = 0
                    if int(score_dpd1(row, int(i[2]), asset, type_of_xml, dpd)) > int(dpd) and \
                            row["acct_type_id"] not in ["A15", "A16", "A17"]:
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
        elif type_of_xml is "CIBIL":
            for dpd_string, dpd_with_asset in zip(row['dpd_with_month'], row["dpd_hist"]):
                trad_dpd = int(str(dpd_string).split('/')[0])
                dpd_asset = str(dpd_with_asset).split('/')[0]
                dpd_date = parser.parse('01-' + str(dpd_string).split('/')[1])
                if req_date <= dpd_date <= today:
                    if int(score_dpd1(row, int(trad_dpd), dpd_asset, type_of_xml, dpd)) > int(dpd) and \
                            row["acct_type_name"] not in ["Loan Extended through Credit Card", "Corporate credit card"]:
                        count = 1
                    else:
                        count = 0
                if count is 1:
                    return 1
            if count is 0:
                return 0
    except:
        return 0


def pol_only_gold_loan_func_hfs(row, type_of_xml):
    try:
        if type_of_xml == "MB":
            if row['acct_type_id'] in ["A09"]:
                return True
            else:
                return False
        elif type_of_xml == "CIBIL":
            if row['acct_type_name'] in ["Gold Loan"]:
                return True
            else:
                return False
    except Exception as error:
        e = error
        return False


# 18
def pol_bl_loan_func_hfs(row, type_of_xml):
    try:
        if float(row['disbursed_amt']) > 50000 and row['acct_type_id'] in ["Unsecured business loan", "A23", "A24", "A25", "A26", "A27", "A28", "A29", "A30", "A31"]:
            return True
        else:
            return False
    except:
        return False


def no_derog_36mths_fun_hfs(row, type_of_xml):
    try:
        if type_of_xml is "MB":
            if row["acct_type_id"] not in ["A15", "A16", "A17"] and (
                    (row['suit_filed_wilful_default'] and int(row['suit_filed_wilful_default']) in range(2, 5)) or int(
                    row['write_off_settled']) in range(5, 16)) and (row['mob'] <= 36):
                return 1
            else:
                return 0
        elif type_of_xml is "EXP":
            if (np.any(row['suit_filed_wilful_default'] == ['89', '93']) or np.any(
                    row['write_off_settled'] == ['30', '31', '97', '32', '33', '34', '35', '36', '37', '38',
                                                 '39'])) and (row['mob'] <= 36):
                return 1
            else:
                return 0
        elif type_of_xml == "CIBIL":
            if row["months_since_open"] <= 36 and row["acct_type_name"] not in ["Corporate credit card", "Loan extended through credit cards"]:
                if str(row['wilful_defaulter']) in ['Wilful Defaulter']:
                    suit_flag = 1
                elif str(row['suit_filed_status']) in ['Suit Filed Case', 'Suit Filed']:
                    suit_flag = 1
                elif str(row['written_off']) in ['Written Off']:
                    suit_flag = 1
                elif str(row["restructure_status"]) in ["Restructure Case"]:
                    suit_flag = 1
                elif str(row["co_status"]) in ["CO Case"]:
                    suit_flag = 1
                else:
                    suit_flag = 0
            else:
                suit_flag = 0
            return suit_flag
    except Exception as error:
        return 0


def pol_no_derog_ever_fun_hfs(row, type_of_xml):
    try:
        if row['suit_filed_wilful_default'] in [None, np.nan]:
            suit_filed_wilful_default = 0
        else:
            suit_filed_wilful_default = int(row['suit_filed_wilful_default'])

        if row['write_off_settled'] in [None, np.nan]:
            write_off_settled = 0
        else:
            write_off_settled = int(row['write_off_settled'])

        if row['write_off_principal'] in [None, np.nan]:
            write_off_principal = 0
        else:
            write_off_principal = int(row['write_off_principal'])

        if row['settlement_amt'] in [None, np.nan]:
            settlement_amt = 0
        else:
            settlement_amt = int(row['settlement_amt'])

        if row['write_off_total'] in [None, np.nan]:
            write_off_total = 0
        else:
            write_off_total = int(row['write_off_total'])

        if row['write_off_principal'] in [None, np.nan]:
            write_off_principal = 0
        else:
            write_off_principal = int(row['write_off_principal'])

        if type_of_xml is "MB":
            if (int(suit_filed_wilful_default) in range(2, 5) or
                                                 int(write_off_settled) in range(5, 16) and
                                                 (settlement_amt > 5000 or write_off_total > 5000 or
                                                  write_off_principal > 5000)):
                return 1
            else:
                return 0
        elif type_of_xml is "EXP":
            if (np.any(suit_filed_wilful_default in [89, 93]) or np.any(
                    write_off_settled in [30, 31, 97, 32, 33, 34, 35, 36, 37, 38, 39]and
                    (settlement_amt > 5000 or write_off_total > 5000 or
                     write_off_principal > 5000))):
                return 1
            else:
                return 0
    except Exception as error:
        return 0


def pol_dpd_plus_mths_disbursed_dt_fun_hfs(row, date_of_request, dpd, months, type_of_xml):
    try:
        count = 0
        disbursed_dt = row["source_date"]
        try:
            date_of_request = datetime.strptime(disbursed_dt, "%Y-%m-%d").date()
        except:
            date_of_request = datetime.strptime(disbursed_dt, "%d-%m-%Y").date()

        for i in row['dpd_dates']:
            i = i.split('-')
            i[0] = datetime.strptime(i[0], '%b').month
            if int(i[0]) in [2, 4, 6, 9, 11] and int(date_of_request.day) > 28:
                d = date(int(i[1]), int(i[0]), 28)
            else:
                d = date(int(i[1]), int(i[0]), int(date_of_request.day))
            request_date = d - dateutil.relativedelta.relativedelta(months=int(months))
            if request_date <= date_of_request <= d:
                asset = i[2].split('/')[1]
                i[2] = i[2].split('/')[0]
                if i[2] in ["xxx", "XXX"]:
                    i[2] = 0
                if int(i[2]) > int(dpd):
                    count = 1
                else:
                    count = 0
            if count is 1:
                return 1
        if count is 0:
            return 0
    except Exception as e:
        return 0


def pol_dpd_plus_mths_disbursed_dt_bal_fun_hfs(row, dpd, months, balance):
    try:
        count = 0
        try:
            date_of_request = datetime.strptime(row["source_date"], "%Y-%m-%d").date()
        except:
            date_of_request = datetime.strptime(row["source_date"], "%d-%m-%Y").date()

        for i in row['dpd_dates']:
            i = i.split('-')
            i[0] = datetime.strptime(i[0], '%b').month
            if int(i[0]) in [2, 4, 6, 9, 11] and int(date_of_request.day) > 28:
                d = date(int(i[1]), int(i[0]), 28)
            else:
                d = date(int(i[1]), int(i[0]), int(date_of_request.day))
            request_date = d - dateutil.relativedelta.relativedelta(months=int(months))
            if request_date <= date_of_request <= d:
                asset = i[2].split('/')[1]
                i[2] = i[2].split('/')[0]
                if i[2] in ["xxx", "XXX"]:
                    i[2] = 0
                if int(i[2]) > int(dpd):
                    count = 1
                else:
                    count = 0
            if count is 1:
                try:
                    return float(balance)
                except:
                    return float(str(balance).replace(",", ""))
        if count is 0:
            return 0
    except Exception as e:
        return 0


def pol_30plus_in_6m_amount_fun_hfs(row, dpd, months, balance):
    try:
        count = 0
        try:
            date_of_request = datetime.strptime(row["source_date"], "%Y-%m-%d").date()
        except:
            date_of_request = datetime.strptime(row["source_date"], "%d-%m-%Y").date()

        request_date = date_of_request - dateutil.relativedelta.relativedelta(months=int(months))

        for i in row['dpd_dates']:
            i = i.split('-')
            i[0] = datetime.strptime(i[0], '%b').month
            if int(i[0]) in [2, 4, 6, 9, 11] and int(date_of_request.day) > 28:
                d = date(int(i[1]), int(i[0]), 28)
            else:
                d = date(int(i[1]), int(i[0]), int(date_of_request.day))
            if request_date <= d <= date_of_request:
                asset = i[2].split('/')[1]
                i[2] = i[2].split('/')[0]
                if i[2] in ["xxx", "XXX"]:
                    i[2] = 0
                if int(i[2]) > int(dpd):
                    count = 1
                else:
                    count = 0
            if count is 1:
                try:
                    return float(balance)
                except:
                    return float(str(balance).replace(",", ""))
        if count is 0:
            return 0
    except Exception as e:
        return 0


def last_max_loan_amount_fun_hfs(tradelines, min_mob):
    try:
        disbursed_amt_list = []
        for row in tradelines:
            if min_mob == row["mob_source_date"]:
                try:
                    disbursed_amt_list.append(float(row["disbursed_amt"]))
                except:
                    disbursed_amt_list.append(float(str(row["disbursed_amt"]).replace(",", "")))
            else:
                disbursed_amt_list.append(0)

        return max(disbursed_amt_list)
    except:
        return 0


def last_max_unsec_loan_amount_fun_hfs(tradelines, min_mob):
    try:
        disbursed_amt_list = []
        for row in tradelines:
            if min_mob == row["mob_source_date"]:
                if row["acct_type_id"] in ['A52', 'A51', 'A05', 'A61', 'A12', 'A54', 'A09', 'A14', 'A55', 'A43', 'A56', 'A40', 'A38']:
                    try:
                        disbursed_amt_list.append(float(row["disbursed_amt"]))
                    except:
                        disbursed_amt_list.append(float(str(row["disbursed_amt"]).replace(",", "")))
                else:
                    disbursed_amt_list.append(0)
            else:
                disbursed_amt_list.append(0)

        return max(disbursed_amt_list)
    except:
        return 0


def last_max_bl_loan_amount_fun_hfs(tradelines, min_mob):
    try:
        disbursed_amt_list = []
        for row in tradelines:
            if min_mob == row["mob_source_date"]:
                if row["acct_type_id"] in ['A52', 'A51', 'A61', 'A54', 'A55', 'A56', 'A40']:
                    try:
                        disbursed_amt_list.append(float(row["disbursed_amt"]))
                    except:
                        disbursed_amt_list.append(float(str(row["disbursed_amt"]).replace(",", "")))
                else:
                    disbursed_amt_list.append(0)
            else:
                disbursed_amt_list.append(0)

        return max(disbursed_amt_list)
    except:
        return 0


def pol_bl_bureau_vintage_fun_hfs(row):
    try:
        if row["acct_type_id"] in ['A52', 'A51', 'A05', 'A61', 'A54', 'A09', 'A55', 'A43', 'A56', 'A40']:
            try:
                disbursed_date = datetime.strptime(row["disbursed_dt"], "%Y-%m-%d").date()
            except:
                disbursed_date = datetime.strptime(row["disbursed_dt"], "%d-%m-%Y").date()

            date_of_request = datetime.strptime(row['source_date'], '%d-%m-%Y').date()

            return abs(math.ceil((disbursed_date - date_of_request).days / 365))
        else:
            return 0
    except:
        return 0


def unsec_perc_loan_amt_fun_hfs(row, amount):
    if row["acct_type_id"] in ['A52', 'A51', 'A05', 'A61', 'A12', 'A54', 'A09', 'A14', 'A55', 'A43', 'A56', 'A40', 'A38']:
        try:
            return float(amount)
        except:
            return float(str(amount).replace(",", ""))
    else:
        return 0


def acct_type_cleaner(acct_type_dict, tradelines):
    msg = None
    for row in tradelines:
        try:
            if int(row['acct_type_id']) == 0:
                row['acct_type_id'] = "A32"
            else:
                row['acct_type_id'] = acct_type_dict[int(row['acct_type_id'])][0]
        except Exception as E:
            logger.info(E)
            row['acct_type_id'] = "A32"
    return tradelines


def live_indicator_fun(row):
    if row['current_bal'] <= 0.0 or row['current_bal'] in ["", None]:
        live_indicator = 0
    else:
        live_indicator = 1
    return live_indicator


def days_calculator(birth_date):
    if birth_date is None or birth_date in [""]:
        return None
    birth_date = birth_date.replace('/', '-').replace(' ', '').replace(',', '')
    if birth_date is None or birth_date in [""]:
        return None
    try:
        birth_date = datetime.strptime(birth_date, '%d-%m-%Y')
        today = datetime.today()
        days = (today - birth_date).days
        return int(days)
    except Exception as exception:
        return None


def inq_date_fun_hfs(arr, dateofreq, days_to_check):
    count = 0
    if arr:
        for i in arr:
            i = datetime.strptime(i, '%d-%m-%Y').date()
            if 0 <= ((dateofreq - i).days) <= int(days_to_check):
                count = count + 1
    return count


def industry_str_cleaner(string):
    if string is not None:
        rm_slash = string.replace(" / ", " ").replace("/ ", " ").replace(" /", " ").replace("/", " ")
        pipe_str = rm_slash.replace(" ", "|").replace("(", "").replace(
            ")", "").replace(",", "").replace("-", "").upper()
        return pipe_str
    else:
        return None


def inq_date_fun(arr, dateofreq, months):
    count = 0
    if arr:
        for i in arr:
            if i is not None:
                try:
                    i = datetime.strptime(i, '%d-%m-%Y').date()
                except:
                    pass
                if 0 <= ((dateofreq - i).days / 30) < int(months):
                    count = count + 1

    return count


def balance_amount_filter(acct_type_id, amount):
    if acct_type_id not in ['A53', 'A36', 'A34']:
        return amount
    else:
        return 0


def calculated_variables(tadelines):
    live_account_list = []
    pol_only_gold_loan_list = []
    pol_bl_loan_list = []
    pol_only_guarantor_list = []
    no_derog_36mths_list = []
    pol_no_derog_ever_list = []
    pol_60_plus_24mth_list = []
    overdue_list = []
    current_balance_list = []
    disbursed_amount_list = []
    unsec_amt_overdue_list = []
    pol_0_plus_3mth_list = []
    pol_0_plus_3mth_req_date_list = []
    mob_source_date_list = []
    bl_bureau_vintage_list = []
    pol_30_plus_3mth_list = []
    unsec_perc_overdue_amt_list = []
    unsec_perc_disbursed_amt_list = []
    unsec_perc_current_bal_list = []
    pol_0_plus_in_6m_curr_balance_list = []
    pol_0_plus_in_6m_disbursed_amt_list = []
    pol_30plus_in_6m_current_bal_list = []
    pol_30plus_in_6m_disbursed_amt_list = []
    pol_90_plus_36mth_list = []
    pol_30_plus_12mth_list = []
    pol_0_plus_6mth_list = []

    for row in tadelines:
        overdue_list.append(overdue_fun_hfs(row))
        current_balance_list.append(balance_amount_filter(row["acct_type_id"], row["current_bal"]))
        disbursed_amount_list.append(balance_amount_filter(row["acct_type_id"], row["disbursed_amt"]))
        unsec_amt_overdue_list.append(unsec_amt_overdue_hfs(row))
        pol_0_plus_3mth_list.append(row['pol_0_plus_3mth'])
        pol_0_plus_3mth_req_date_list.append(row["pol_0_plus_3mth_req_date"])
        mob_source_date_list.append(row["mob_source_date"])
        bl_bureau_vintage_list.append(pol_bl_bureau_vintage_fun_hfs(row))
        unsec_perc_overdue_amt_list.append(unsec_perc_loan_amt_fun_hfs(row, row["overdue_amt"]))
        unsec_perc_disbursed_amt_list.append(unsec_perc_loan_amt_fun_hfs(row, row["disbursed_amt"]))
        pol_0_plus_in_6m_curr_balance_list.append(pol_dpd_plus_mths_disbursed_dt_bal_fun_hfs(row, 0, 6, row["current_bal"]))
        pol_0_plus_in_6m_disbursed_amt_list.append(pol_dpd_plus_mths_disbursed_dt_bal_fun_hfs(row, 0, 6, row["disbursed_amt"]))
        unsec_perc_current_bal_list.append(unsec_perc_loan_amt_fun_hfs(row, row["current_bal"]))
        pol_30_plus_3mth_list.append(row['pol_30_plus_3mth'])
        pol_30plus_in_6m_current_bal_list.append(pol_30plus_in_6m_amount_fun_hfs(row, 30, 6, row["current_bal"]))
        pol_30plus_in_6m_disbursed_amt_list.append(pol_30plus_in_6m_amount_fun_hfs(row, 30, 6, row["disbursed_amt"]))
        # ========= HFS Response variable calc =========
        no_derog_36mths_list.append(row['no_derog_36mths'])
        pol_no_derog_ever_list.append(row['pol_no_derog_ever'])
        pol_90_plus_36mth_list.append(row["pol_90_plus_36mth"])
        pol_60_plus_24mth_list.append(row['pol_60_plus_24mth'])
        pol_30_plus_12mth_list.append(row["pol_30_plus_12mth"])
        pol_0_plus_6mth_list.append(row["pol_0_plus_6mth"])
        live_account_list.append(0 if row['live_account'] is None else row['live_account'])
        pol_only_gold_loan_list.append(row['pol_only_gold_loan'])
        pol_bl_loan_list.append(row['pol_bl_loan'])
        pol_only_guarantor_list.append(row['pol_only_guarantor'])

    return live_account_list, pol_only_gold_loan_list, pol_bl_loan_list, pol_only_guarantor_list, \
        no_derog_36mths_list, pol_no_derog_ever_list, pol_60_plus_24mth_list, overdue_list, \
        current_balance_list, disbursed_amount_list, unsec_amt_overdue_list, \
        pol_0_plus_3mth_list, mob_source_date_list, bl_bureau_vintage_list, \
        pol_30_plus_3mth_list, unsec_perc_overdue_amt_list, unsec_perc_disbursed_amt_list, \
        unsec_perc_current_bal_list, pol_0_plus_in_6m_curr_balance_list, \
        pol_0_plus_in_6m_disbursed_amt_list, pol_30plus_in_6m_current_bal_list, \
        pol_30plus_in_6m_disbursed_amt_list, pol_90_plus_36mth_list, \
        pol_30_plus_12mth_list, pol_0_plus_6mth_list, pol_0_plus_3mth_req_date_list


def date_cleaner(enq_date):
    if enq_date is not None:
        try:
            request_date = datetime.strptime(enq_date, '%d-%b-%Y')
        except:
            request_date = datetime.strptime(enq_date, '%d-%m-%Y')
        return request_date
    else:
        return None


def check_no_hit_with_months_hfs(sourcing_dates, tradelines, months):
    try:
        disbursed_dates = [date_cleaner(x['disbursed_dt']) for x in tradelines]
        min_disbursed_date = min([i for i in disbursed_dates if i not in [None]])

        req_date = sourcing_dates - dateutil.relativedelta.relativedelta(months=int(months))
        if req_date < min_disbursed_date < sourcing_dates:
            return True
        else:
            return False
    except Exception as error:
        return True


def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)


def commercial_parser(bureau_json):
    data_dict = {}
    data_dict['sourcing_date'] = deep_get(bureau_json, 'base.responseReport.reportHeaderRec.reportOrderDate')
    data_dict['request_id'] = deep_get(bureau_json, 'base.responseReport.reportHeaderRec.reportOrderNumber')
    data_dict['los_app_id'] = deep_get(bureau_json, 'base.responseReport.reportHeaderRec.applicationReferenceNumber')
    data_dict['crif_score'] = deep_get(bureau_json, 'base.responseReport.productSec.rankSec.rankValue')
    idx = 0
    tradelines = []
    for trad_json in deep_get(bureau_json, 'base.responseReport.productSec.creditFacilityDetailsasBorrowerSecVec.creditFacilityDetailsasBorrowerSec'):
        row = {}
        row['acct_type_name'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfType')
        row['acct_type_group'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfMember')
        row['current_asset_class'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.assetClassificationDaysPastDueDpd')

        row['currency'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.currency')

        row['disbursed_dt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.sanctionedDt')

        row['sanctioned_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.sanctionedAmt')
        row['drawing_power'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.drawingPower')
        row['current_bal'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.outstandingBalance')
        row['overdue_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.overdue')
        row['write_off_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.writtenOFF')
        row['wilful_defaulter'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.wilfulDefault')
        row['suit_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.suitFiledAmt')
        row['credit_status'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.status')

        dpd_value, dpd_month = '', ''
        dpd_trad_list = []
        for dpd_trad in deep_get(trad_json, 'CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months'):
            dpd_trad_list.append(f"{deep_get(dpd_trad, 'ACorDPD')}/{deep_get(dpd_trad, 'month')}")

        row['dpd_hist'] = dpd_trad_list

        row['tradeline_no'] = idx
        tradelines.append(row)
        idx += 1
        row = {}
        row['acct_type_name'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfType')
        row['acct_type_group'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfMember')
        row['current_asset_class'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.assetClassificationDaysPastDueDpd')

        row['currency'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.currency')

        row['disbursed_dt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.sanctionedDt')

        row['sanctioned_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.sanctionedAmt')
        row['drawing_power'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.drawingPower')
        row['current_bal'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.outstandingBalance')
        row['overdue_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.overdue')
        row['write_off_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.writtenOFF')
        row['wilful_defaulter'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.wilfulDefault')
        row['suit_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.suitFiledAmt')
        row['credit_status'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.status')

        dpd_value, dpd_month = '', ''
        dpd_trad_list = []
        for dpd_trad in deep_get(trad_json, 'CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months'):
            dpd_trad_list.append(f"{deep_get(dpd_trad, 'ACorDPD')}/{deep_get(dpd_trad, 'month')}")

        row['dpd_hist'] = dpd_trad_list

        row['tradeline_no'] = idx
    data_dict["tradelines"] = tradelines

    return data_dict


class Commercial(object):
    def __init__(self, cibil_json):
        self.cibil_json = cibil_json
        self.idx = 0
        self.crif_score = None
        self.sourcing_date = None
        self.request_id = None
        self.los_app_id = None
        self.report_number = None
        self.loan_amount = None
        self.tradelines = []
        self.num_of_tradeline = 0
        self.enquiry_tradeline = []
        self.get_prime_var()
        self.get_trad()

    def get_prime_var(self):
        self.sourcing_date = deep_get(self.cibil_json, 'base.responseReport.reportHeaderRec.reportOrderDate')
        self.request_id = deep_get(self.cibil_json, 'base.responseReport.reportHeaderRec.reportOrderNumber')
        self.los_app_id = deep_get(self.cibil_json, 'base.responseReport.reportHeaderRec.applicationReferenceNumber')
        crif_score = deep_get(self.cibil_json, 'base.responseReport.productSec.rankSec.rankValue')
        self.crif_score = crif_score if crif_score not in ["NONE", None, 'None', ""] else 0

    def get_trad(self):
        is_tradeline = False
        for trad_json in deep_get(self.cibil_json, 'base.responseReport.productSec.creditFacilityDetailsasBorrowerSecVec.creditFacilityDetailsasBorrowerSec'):
            row = {}
            is_tradeline = True
            row['acct_type_name'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfType')
            row['acct_type_group'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.cfMember')
            row['current_asset_class'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.assetClassificationDaysPastDueDpd')
            row['currency'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.currency')

            row['disbursed_dt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.sanctionedDt')

            row['sanctioned_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.sanctionedAmt')
            row['drawing_power'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.drawingPower')
            row['current_bal'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.outstandingBalance')
            row['overdue_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.overdue')
            row['write_off_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.writtenOFF')
            row['wilful_defaulter_date'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.dates.wilfulDefault')

            row['suit_amt'] = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.amount.suitFiledAmt')
            status = deep_get(trad_json, 'creditFacilityCurrentDetailsVec.creditFacilityCurrentDetails.status')
            row['credit_status'] = status
            row["account_status"] = 'Open' if 'Open' in status else 'Closed'

            if 'Not Wilful Defaulter' in status or 'Not a Wilful Defaulter' in status:
                row["wilful_defaulter"] = 'Not Wilful Defaulter'
            elif ('Wilful Defaulter' in status) or ('Wilful Defaulter Case' in status):
                row["wilful_defaulter"] = 'Wilful Defaulter'
            else:
                try:
                    wilful_defaulter_list = [i for i in str(status).split(",") if 'Wilful' in i][0].split(" ")
                    if 'Wilful' in wilful_defaulter_list and 'Not' not in wilful_defaulter_list:
                        row["wilful_defaulter"] = 'Wilful Defaulter'
                    else:
                        row["wilful_defaulter"] = None
                except:
                    row["wilful_defaulter"] = None

            try:
                written_list = [i for i in str(status).split(",") if 'Written' in i][0].split(" ")
            except:
                written_list = []

            if 'Written' in written_list and 'Not' not in written_list:
                row["written_off"] = 'Written Off'
            else:
                row["written_off"] = None


            try:
                co_list = [i for i in str(status).split(",") if 'CO' in i][0].split(" ")
            except:
                co_list = []

            if 'CO' in co_list and 'Not' not in co_list:
                row["co_status"] = 'CO Case'
            else:
                row["co_status"] = None

            if 'Not a Suit Filed Case' in status or ' Suit Filed Case' in status:
                row["suit_filed_status"] = 'Not a Suit Filed Case'
            elif 'Suit Filed Case' in status:
                row["suit_filed_status"] = 'Suit Filed Case'
            else:
                try:
                    suit_filed_list = [i for i in str(status).split(",") if 'Suit Filed' in i][0].split(" ")
                    if 'Suit' in suit_filed_list and 'Filed' in suit_filed_list and 'Not' not in suit_filed_list:
                        row["suit_filed_status"] = 'Suit Filed Case'
                    else:
                        row["suit_filed_status"] = None
                except:
                    row["suit_filed_status"] = None

            if 'Not a Restructure' in status or 'Not Restructure' in status:
                row["restructure_status"] = 'Not a Restructure Case'
            elif 'Restructure' in status:
                row["restructure_status"] = 'Restructure Case'
            else:
                try:
                    restructure_list = [i for i in str(status).split(",") if 'Restructure' in i][0].split(" ")
                    if 'Restructure' in restructure_list and 'Not' not in restructure_list:
                        row["restructure_status"] = 'Restructure Case'
                    else:
                        row["restructure_status"] = None
                except:
                    row["restructure_status"] = None

            dpd_value, dpd_month = '', ''
            dpd_trad_list = []
            for dpd_trad in deep_get(trad_json, 'CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months'):
                dpd_trad_list.append(f"{deep_get(dpd_trad, 'ACorDPD')}/{deep_get(dpd_trad, 'month')}")

            row['dpd_hist'] = dpd_trad_list

            try:
                row["nbr_reportings"] = len(deep_get(trad_json, 'CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months'))
            except:
                row["nbr_reportings"] = 24

            row['tradeline_no'] = self.idx + 1
            self.tradelines.append(row)
            self.idx += 1

        self.num_of_tradeline = self.idx
        for inq in deep_get(self.cibil_json, 'base.responseReport.productSec.enquiryDetailsInLast24MonthVec.enquiryDetailsInLast24Month'):
            inq["enquiryAmt"] = str(inq["enquiryAmt"]).replace(" ", "")
            inq["enquiryDt"] = date_cleaner(inq["enquiryDt"])
            self.enquiry_tradeline.append(inq)


def eligible_loan_amt_cal(final_band, abb, final_tenure, final_roi, debitable_abb):

    if str(debitable_abb).upper() not in ["EMI"]:
        if final_band in ["S1", 1, '1']:
            final_emi = abb/1.25
            abb_to_emi = 1.25
        elif final_band in ["S2", 2, '2']:
            final_emi = abb/1.50
            abb_to_emi = 1.50
        elif final_band in ["S3", 3, '3']:
            final_emi = abb/2.00
            abb_to_emi = 2.00
        elif final_band in ["S4", 4, '4']:
            final_emi = abb/3.00
            abb_to_emi = 3.00
        else:
            final_emi = 0
            abb_to_emi = 0
    else:
        final_emi = abb/1.25
        abb_to_emi = 1.25

    final_emi_loan_amount = float(abs(np.pv(rate=final_roi / 1200, nper=final_tenure, fv=0, pmt=-1 * final_emi)))
    return final_emi_loan_amount, final_emi, abb_to_emi


def get_final_eligibility(final_response, loan_details, app_banking_data, asset_type_cat, total_asset_value, asset_condition, asset_type):
    final_band = final_response.get("final_band")
    roi = float(loan_details.get("roi"))
    requested_tenure = float(loan_details.get("tenure"))
    loan_amount = float(loan_details.get("loanAmount"))
    try:
        sd_amount = float(loan_details.get("ltvDetails", {}).get("securityDepositAmt", 0))
    except:
        sd_amount = 0

    debitable_abb = str(loan_details.get("deviations", {}).get("Debitable ABB"))

    try:
        abb = float(app_banking_data["abb"])
    except:
        abb = 0
    pol_eligibility_decision = "PASS"
    eligibility_reject_reasons = []

    if final_band in ["S1", "S2", "S3", "S4"] and asset_type_cat in ["cat a", "cat b"]:
        grid_tenure = 60
    elif final_band in ["S1", "S2"] and asset_type_cat in ["cat c"]:
        grid_tenure = 60
    elif final_band in ["S3"] and asset_type_cat in ["cat c"]:
        grid_tenure = 42
    elif final_band in ["S4"] and asset_type_cat in ["cat c"]:
        grid_tenure = 36
    else:
        grid_tenure = 0

    final_tenure = min(grid_tenure, requested_tenure)

    final_emi_loan_amount, final_abb_emi, abb_to_emi = eligible_loan_amt_cal(final_band, abb, final_tenure, roi, debitable_abb)
    initial_abb_loan_amount = final_emi_loan_amount

    # if final_emi_loan_amount < loan_amount and final_band not in ["S1"]:
    #     final_band_int = int(str(final_band).replace("S", ""))
    #     for i in range(final_band_int, 0, -1):
    #         final_band_int -= 1
    #         if final_band_int > 0:
    #             final_emi_loan_amount, final_abb_emi = eligible_loan_amt_cal(final_band_int, abb, final_tenure, roi)
    #             if final_emi_loan_amount >= loan_amount:
    #                 pol_eligibility_decision = "RTC"
    #                 break

    # final_loan_amount = min(final_emi_loan_amount, loan_amount)

    ltv_on_abb_loan_amt = total_asset_value and float("{0:.4f}".format(final_emi_loan_amount/total_asset_value))*100 or 0

    if asset_condition in ["refurbished"] and asset_type in ["CT", "MRI"] and final_band in ["S1", "S2", "S3", "S4"]:
        ltv_asset = 75
    elif final_band in ["S1", "S2"] and asset_type_cat in ["cat a", "cat b"]:
        ltv_asset = 90
    elif final_band in ["S1", "S2"] and asset_type_cat in ["cat c"]:
        ltv_asset = 85
    elif final_band in ["S3", "S4"] and asset_type_cat in ["cat a", "cat b"]:
        ltv_asset = 85
    elif final_band in ["S3"] and asset_type_cat in ["cat c"]:
        ltv_asset = 80
    elif final_band in ["S4"] and asset_type_cat in ["cat c"]:
        ltv_asset = 75
    else:
        ltv_asset = 0

    # if ltv > ltv_asset:
    #     loan_amount_net_ltv = (total_asset_value*ltv_asset)/100
    # else:
    #     loan_amount_net_ltv = final_emi_loan_amount

    loan_amount_asset_ltv = (total_asset_value*ltv_asset)/100

    eligible_loan_amount = float("{:.3f}".format(min(final_emi_loan_amount, loan_amount_asset_ltv)))

    min_loan_amount_asset_emi = min(total_asset_value, final_emi_loan_amount)

    if min_loan_amount_asset_emi > eligible_loan_amount:
        diff_ltv_emi_amt = min_loan_amount_asset_emi - eligible_loan_amount
        if diff_ltv_emi_amt <= sd_amount:
            final_loan_amount = eligible_loan_amount + diff_ltv_emi_amt
            revised_sd_amount = diff_ltv_emi_amt
        else:
            final_loan_amount = eligible_loan_amount + sd_amount
            revised_sd_amount = sd_amount
    else:
        final_loan_amount = min_loan_amount_asset_emi
        revised_sd_amount = 0

    final_loan_amount = float("{:.3f}".format(min(total_asset_value, final_loan_amount)))

    final_loan_amount = min(final_loan_amount, loan_amount)

    if final_loan_amount > 5000000 and final_band in ["S1", "S2"]:
        final_loan_amount = 5000000
    elif final_loan_amount > 3000000 and final_band in ["S3", "S4"]:
        final_loan_amount = 3000000

    final_emi = (final_loan_amount and roi and final_tenure) and float(
        round(abs(float(np.pmt((roi / 1200), final_tenure, -final_loan_amount))),
              2)) or 0
    if math.isnan(final_emi) or math.isinf(final_emi):
        final_emi = 0

    if not all([final_loan_amount, final_emi, final_tenure, roi]):
        final_loan_amount, roi, final_tenure, final_emi = 0, 0, 0, 0

    final_loan_amount_net = min(eligible_loan_amount, final_loan_amount)

    net_ltv = total_asset_value and float("{0:.4f}".format(final_loan_amount_net/total_asset_value))*100 or 0

    # if 100000 <= final_loan_amount < loan_amount:
    #     pol_eligibility_decision = "RTC"
    #     eligibility_reject_reasons = ["final loan amount is less than requested loan amount"]
    if final_loan_amount < 100000 or loan_amount < 100000:
        pol_eligibility_decision = "FAIL"
        if loan_amount < 100000:
            eligibility_reject_reasons = [f"requested loan amount {loan_amount} is less than 1Lac"]
        else:
            eligibility_reject_reasons = [f"final loan amount: {final_loan_amount} is less than 1Lac"]

    gross_ltv = total_asset_value and float("{0:.4f}".format(final_loan_amount/total_asset_value))*100 or 0

    final_abb_to_emi = final_emi and float("{0:.2f}".format(abb/final_emi)) or 0

    return {"final_roi": roi, "final_tenure": final_tenure, "final_emi": round(final_emi),
            "final_loan_amount": round(final_loan_amount)}, round(final_abb_emi), abb_to_emi, net_ltv, gross_ltv, ltv_asset, pol_eligibility_decision, \
        eligibility_reject_reasons, final_emi_loan_amount, initial_abb_loan_amount, loan_amount_asset_ltv, \
           {"sd_amount": sd_amount, "revised_sd_amount": revised_sd_amount, "debitable_abb": debitable_abb, "final_abb_to_emi": final_abb_to_emi, "eligible_loan_amount(min)": eligible_loan_amount, "final_loan_amount_net": final_loan_amount_net}
