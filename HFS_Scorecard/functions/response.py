import os
import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

'''
    1. Applicant & co-applicant type both can't be COMPANY(commercial).
    2. If applicant or co-applicant type is INDIVIDUAL(consumer) or COMPANY(commercial) vice-versa then 
        below if-else condition is useful.  (Line No: 30 or 35)
    3. When both applicant & co-applicant type is INDIVIDUAL(consumer) then
        we consider consumer Score as Max of all - Higher Risk. (Line No: 48)
'''


def get_final_decision(app_bureau_data, co_app_bureau_data, app_banking_data,
                       app_co_app_qde_data, app_score_band, co_app_score_band,
                       banking_demographic_score_band, demographic_score_band, is_applicant_ntc,
                       is_co_applicant_ntc, is_co_applicant, is_eligible_for_bank_demographic,
                       is_eligible_for_bureau_bank_demo):

    reasons = []
    risk_dict = {1: "A1", 2: "A2", 3: "A3", 4: "A4", 5: "A5"}
    try:
        app_score_band_int = int(app_score_band)
    except:
        app_score_band_int = 5
    try:
        co_app_score_band_int = int(co_app_score_band)
    except:
        co_app_score_band_int = 5
    app_score_band = risk_dict[app_score_band_int] if app_score_band_int in risk_dict else app_score_band_int
    co_app_score_band = risk_dict[co_app_score_band_int] if co_app_score_band_int in risk_dict else \
        co_app_score_band_int

    consumer_score_band = None
    commercial_score_band = None
    if app_bureau_data["user_type"] == "INDIVIDUAL":
        consumer_score_band = app_score_band
    elif app_bureau_data["user_type"] == "COMPANY":
        commercial_score_band = app_score_band

    if is_co_applicant and co_app_bureau_data["user_type"] == 'INDIVIDUAL':
        consumer_score_band = co_app_score_band

    final_bureau_band = "B4"
    cross_tab_2 = "BB4"
    final_decision_band = "S5"

    if app_banking_data is not None and app_co_app_qde_data is not None:
        if is_eligible_for_bureau_bank_demo is True:
            if (app_bureau_data["user_type"] == "INDIVIDUAL" and is_co_applicant is False) or (app_bureau_data["user_type"] == "INDIVIDUAL" and co_app_bureau_data["user_type"] == "INDIVIDUAL"):
                if is_co_applicant is True and app_score_band_int < co_app_score_band_int:
                    final_bureau_band = co_app_score_band
                else:
                    final_bureau_band = app_score_band

                if final_bureau_band in ["A1", "A2"]:
                    if banking_demographic_score_band in ["BN1", "BN2"]:
                        cross_tab_2 = "BB1"
                    elif banking_demographic_score_band in ["BN3"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN4", "BN5"]:
                        cross_tab_2 = "BB3"
                elif final_bureau_band in ["A3"]:
                    if banking_demographic_score_band in ["BN1", "BN2"]:
                        cross_tab_2 = "BB1"
                    elif banking_demographic_score_band in ["BN3"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN4", "BN5"]:
                        cross_tab_2 = "BB3"
                elif final_bureau_band in ["A4"]:
                    if banking_demographic_score_band in ["BN1"]:
                        cross_tab_2 = "BB1"
                    elif banking_demographic_score_band in ["BN2"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN3", "BN4"]:
                        cross_tab_2 = "BB3"
                    elif banking_demographic_score_band in ["BN5"]:
                        cross_tab_2 = "BB4"
                elif final_bureau_band in ["A5"]:
                    if banking_demographic_score_band in ["BN1"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN2"]:
                        cross_tab_2 = "BB3"
                    elif banking_demographic_score_band in ["BN3", "BN4", "BN5"]:
                        cross_tab_2 = "BB4"

            elif app_bureau_data["user_type"] in ["COMPANY"] and co_app_bureau_data["user_type"] in ["INDIVIDUAL"]:
                if commercial_score_band in ["A1"]:
                    if consumer_score_band in ["A1", "A2"]:
                        final_bureau_band = "B1"
                    elif consumer_score_band in ["A3", "A4"]:
                        final_bureau_band = "B2"
                    elif consumer_score_band in ["A5"]:
                        final_bureau_band = "B3"
                elif commercial_score_band in ["A2"]:
                    if consumer_score_band in ["A1", "A2"]:
                        final_bureau_band = "B1"
                    elif consumer_score_band in ["A3", "A4"]:
                        final_bureau_band = "B2"
                    elif consumer_score_band in ["A5"]:
                        final_bureau_band = "B3"
                elif commercial_score_band in ["A3"]:
                    if consumer_score_band in ["A1", "A2"]:
                        final_bureau_band = "B1"
                    elif consumer_score_band in ["A3"]:
                        final_bureau_band = "B2"
                    elif consumer_score_band in ["A4", "A5"]:
                        final_bureau_band = "B3"
                elif commercial_score_band in ["A4"]:
                    if consumer_score_band in ["A1", "A2"]:
                        final_bureau_band = "B2"
                    elif consumer_score_band in ["A3", "A4"]:
                        final_bureau_band = "B3"
                    elif consumer_score_band in ["A5"]:
                        final_bureau_band = "B4"
                elif commercial_score_band in ["A5"]:
                    if consumer_score_band in ["A1", "A2"]:
                        final_bureau_band = "B3"
                    elif consumer_score_band in ["A3", "A4", "A5"]:
                        final_bureau_band = "B4"

                if final_bureau_band in ["B1", "B2"]:
                    if banking_demographic_score_band in ["BN1", "BN2"]:
                        cross_tab_2 = "BB1"
                    elif banking_demographic_score_band in ["BN3"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN4", "BN5"]:
                        cross_tab_2 = "BB3"
                elif final_bureau_band in ["B3"]:
                    if banking_demographic_score_band in ["BN1"]:
                        cross_tab_2 = "BB1"
                    elif banking_demographic_score_band in ["BN2"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN3", "BN4"]:
                        cross_tab_2 = "BB3"
                    elif banking_demographic_score_band in ["BN5"]:
                        cross_tab_2 = "BB4"
                elif final_bureau_band in ["B4"]:
                    if banking_demographic_score_band in ["BN1"]:
                        cross_tab_2 = "BB2"
                    elif banking_demographic_score_band in ["BN2"]:
                        cross_tab_2 = "BB3"
                    elif banking_demographic_score_band in ["BN3", "BN4", "BN5"]:
                        cross_tab_2 = "BB4"

            if cross_tab_2 in ["BB1"]:
                if demographic_score_band in ["D1", "D2"]:
                    final_decision_band = "S1"
                elif demographic_score_band in ["D3"]:
                    final_decision_band = "S2"
                elif demographic_score_band in ["D4"]:
                    final_decision_band = "S3"
                elif demographic_score_band in ["D5"]:
                    final_decision_band = "S4"
            elif cross_tab_2 in ["BB2"]:
                if demographic_score_band in ["D1", "D2"]:
                    final_decision_band = "S1"
                elif demographic_score_band in ["D3"]:
                    final_decision_band = "S2"
                elif demographic_score_band in ["D4"]:
                    final_decision_band = "S3"
                elif demographic_score_band in ["D5"]:
                    final_decision_band = "S4"
            elif cross_tab_2 in ["BB3"]:
                if demographic_score_band in ["D1", "D2"]:
                    final_decision_band = "S2"
                elif demographic_score_band in ["D3"]:
                    final_decision_band = "S3"
                elif demographic_score_band in ["D4"]:
                    final_decision_band = "S4"
                elif demographic_score_band in ["D5"]:
                    final_decision_band = "S5"
            elif cross_tab_2 in ["BB4"]:
                if demographic_score_band in ["D1"]:
                    final_decision_band = "S3"
                elif demographic_score_band in ["D2"]:
                    final_decision_band = "S4"
                elif demographic_score_band in ["D3", "D4", "D5"]:
                    final_decision_band = "S5"
        elif is_eligible_for_bank_demographic is True:
            if banking_demographic_score_band in ["BN1"]:
                if demographic_score_band in ["D1", "D2"]:
                    final_decision_band = "S2"
                elif demographic_score_band in ["D3", "D4", "D5"]:
                    final_decision_band = "S4"
                else:
                    final_decision_band = "S5"
            elif banking_demographic_score_band in ["BN2"]:
                if demographic_score_band in ["D1", "D2"]:
                    final_decision_band = "S2"
                elif demographic_score_band in ["D3", "D4", "D5"]:
                    final_decision_band = "S4"
                else:
                    final_decision_band = "S5"
            elif banking_demographic_score_band in ["BN3"]:
                final_decision_band = "S4"
            elif banking_demographic_score_band in ["BN4", "BN5"]:
                final_decision_band = "S5"
        else:
            if is_applicant_ntc and co_app_score_band in ["A5", 5]:
                reasons.append("Applicant is NTC and Co-Applicant band is A5")
            elif is_co_applicant_ntc and app_score_band in ["A5", 5]:
                reasons.append("Applicant band is A5 and Co-Applicant is NTC")
            else:
                reasons.append("Not eligible for any scorecard band")

            final_decision_band = "S5"

        if app_bureau_data is not None and app_bureau_data["pol_decision_status"] == "Accept" and \
                co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] == "Accept" and \
                app_banking_data is not None and app_banking_data["pol_decision_status"] == "Accept" and \
                app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] == "Accept":

            if is_applicant_ntc is False and is_co_applicant_ntc is False and is_eligible_for_bureau_bank_demo:
                if final_decision_band in ["S1", "S2", "S3", "S4"]:
                    pol_final_decision_status = "Accept"
                else:
                    reasons.append(f"final_decision_band is {final_decision_band}")
                    pol_final_decision_status = "Decline"
            else:
                if is_eligible_for_bank_demographic:
                    if final_decision_band in ["S2"]:
                        pol_final_decision_status = "Accept"
                    elif final_decision_band in ["S4"]:
                        pol_final_decision_status = "Refer"
                        reasons.append(f"final_decision_band is {final_decision_band} or bureau is NTC")
                    else:
                        reasons.append(f"final_decision_band is {final_decision_band}")
                        pol_final_decision_status = "Decline"
                else:
                    reasons.append(f"final_decision_band is {final_decision_band}")
                    pol_final_decision_status = "Decline"

            return {"status": pol_final_decision_status, "reasons": reasons, "final_band": final_decision_band}

        elif app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Decline"] or \
                co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Decline"] or \
                app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Decline"] or \
                app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Decline"]:

            app_bureau_status = None
            co_app_bureau_status = None
            if is_applicant_ntc is False and is_co_applicant_ntc is False:
                if app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Decline"]:
                    reasons += app_bureau_data["rejection_reason"]
                    app_bureau_status = "Decline"
                else:
                    if app_bureau_data is None:
                        reasons.append("Applicant bureau data is none")
                        app_bureau_status = "Decline"
                    elif app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Refer", "RTC"]:
                        app_bureau_status = "Refer"
                        reasons += app_bureau_data["rejection_reason"]
                    elif app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Accept"]:
                        app_bureau_status = "Accept"
                    else:
                        reasons += app_bureau_data["rejection_reason"]
                        app_bureau_status = "Decline"

                if co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Decline"]:
                    reasons += co_app_bureau_data["rejection_reason"]
                    co_app_bureau_status = "Decline"
                else:
                    if co_app_bureau_data is None:
                        reasons.append("Co-Applicant bureau data is none")
                        co_app_bureau_status = "Decline"
                    elif co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Refer", "RTC"]:
                        co_app_bureau_status = "Refer"
                        reasons += co_app_bureau_data["rejection_reason"]
                    elif co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Accept"]:
                        co_app_bureau_status = "Accept"
                    else:
                        reasons += co_app_bureau_data["rejection_reason"]
                        co_app_bureau_status = "Decline"

            if app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Decline"]:
                reasons += app_banking_data["rejection_reason"]
                banking_status = "Decline"
            else:
                if app_banking_data is None:
                    reasons.append("Applicant banking is none")
                    banking_status = "Decline"
                elif app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Refer", "RTC"]:
                    reasons += app_banking_data["rejection_reason"]
                    banking_status = "Refer"
                elif app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Accept"]:
                    banking_status = "Accept"
                else:
                    banking_status = "Decline"
                    reasons += app_banking_data["rejection_reason"]

            if app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Decline"]:
                reasons += app_co_app_qde_data["rejection_reason"]
                qde_status = "Decline"
            else:
                if app_co_app_qde_data is None:
                    qde_status = "Decline"
                    reasons.append("Application demographic data is none")
                elif app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Refer", "RTC"]:
                    reasons += app_co_app_qde_data["rejection_reason"]
                    qde_status = "Refer"
                elif app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Accept"]:
                    qde_status = "Accept"
                else:
                    reasons += app_co_app_qde_data["rejection_reason"]
                    qde_status = "Decline"

            if is_applicant_ntc is False and is_co_applicant_ntc is False:
                if final_decision_band not in ["S1", "S2", "S3", "S4"]:
                    reasons.append(f"final_decision_band is {final_decision_band}")
            else:
                if final_decision_band in ["S4", "S5"]:
                    reasons.append(f"final_decision_band is {final_decision_band} and bureau is NTC")

            if is_applicant_ntc is False and is_co_applicant_ntc is False:
                if banking_status == "Accept" and \
                        qde_status == "Accept" and \
                        app_bureau_status == "Accept" and \
                        co_app_bureau_status == "Accept" and final_decision_band in ["S1", "S2", "S3", "S4"]:
                    final_status = "Accept"
                elif banking_status == "Decline" or \
                        qde_status == "Decline" or \
                        app_bureau_status == "Decline" or \
                        co_app_bureau_status == "Decline":
                    final_status = "Decline"
                elif banking_status in ["Refer", "RTC"] or \
                        qde_status in ["Refer", "RTC"] or \
                        app_bureau_status in ["Refer", "RTC"] or \
                        co_app_bureau_status in ["Refer", "RTC"]:
                    final_status = "Refer"
                else:
                    final_status = "Decline"
            else:
                if banking_status == "Accept" and qde_status == "Accept" and final_decision_band in ["S2"]:
                    final_status = "Accept"
                elif banking_status == "Decline" or qde_status == "Decline":
                    final_status = "Decline"
                elif banking_status in ["Refer", "RTC"] or qde_status in ["Refer", "RTC"] or final_decision_band in ["S4"]:
                    final_status = "Refer"
                else:
                    final_status = "Decline"

            return {"status": final_status, "reasons": reasons, "final_band": final_decision_band}

        elif app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Refer", "RTC"] or \
                co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Refer", "RTC"] or \
                app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Refer", "RTC"] or \
                app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Refer", "RTC"]:

            if is_applicant_ntc is False and is_co_applicant_ntc is False:
                if app_bureau_data is not None and app_bureau_data["pol_decision_status"] in ["Refer", "RTC"]:
                    reasons += app_bureau_data["rejection_reason"]
                else:
                    if app_bureau_data is None:
                        reasons.append("Applicant bureau data is none")

                if co_app_bureau_data is not None and co_app_bureau_data["pol_decision_status"] in ["Refer", "RTC"]:
                    reasons += co_app_bureau_data["rejection_reason"]
                else:
                    if co_app_bureau_data is None:
                        reasons.append("Co-Applicant bureau data is none")

            if app_banking_data is not None and app_banking_data["pol_decision_status"] in ["Refer", "RTC"]:
                reasons += app_banking_data["rejection_reason"]
            else:
                if app_banking_data is None:
                    reasons.append("Applicant banking data is none")

            if app_co_app_qde_data is not None and app_co_app_qde_data["pol_decision_status"] in ["Refer", "RTC"]:
                reasons += app_co_app_qde_data["rejection_reason"]
            else:
                if app_co_app_qde_data is None:
                    reasons.append("Application demographic data is none")

            return {"status": "Refer", "reasons": reasons, "final_band": final_decision_band}

        else:
            if app_bureau_data is None:
                reasons.append("APPLICANT Data is None")
            if co_app_bureau_data is None:
                reasons.append("CO-APPLICANT Data is None")

            if app_banking_data is None:
                reasons.append("APPLICANT Banking Data is None")

            if app_co_app_qde_data is None:
                reasons.append("APPLICANT or CO-APPLICANT QDE Data is None")

            return {"status": "Decline", "reasons": reasons, "final_band": final_decision_band}

    else:
        if app_bureau_data is None:
            reasons.append("APPLICANT Bureau Data is None")
        else:
            reasons += app_bureau_data["rejection_reason"]

        if app_banking_data is None:
            reasons.append("APPLICANT Banking Data is None")
        else:
            reasons += app_banking_data["rejection_reason"]

        if app_co_app_qde_data is None:
            reasons.append("Application demographic data is none")
        else:
            reasons += app_co_app_qde_data["rejection_reason"]

        return {"status": "Decline", "reasons": reasons, "final_band": final_decision_band}


def warning_resp_json(event, env, warnings):
    return {"status": False,
                "loanApplicationId": event["applicationId"],
                "customerId": event["customerId"],
                "product": event["product"],
                "partner": event["partner"],
                "stage": event["report"]["stage"]["applicationStage"].lower(),
                "scorecard": {
                    "scorecard_name": f"{env}:D2C_Scorecard",
                    "scorecard_version": "1.0"
                },
                "warnings": warnings}


def response_json(request, response, pol_eligibility_decision, eligibility_reject_reasons, app_score_band, co_app_score_band, app_bureau_data,
                  co_applicant_data_list, app_banking_data, app_co_app_qde_data,
                  banking_demographic_score_band, demographic_score_band, final_eligibility, final_emi_loan_amount,
                  initial_abb_loan_amount, loan_amount_asset_ltv, final_abb_emi, abb_to_emi, ltv,
                  asset_type_cat, gross_ltv, ltv_asset, common_var_dict, max_perc_collateral, bureau_score, pol_bureau_score,
                  is_applicant_ntc, is_co_applicant_ntc, is_co_applicant,
                  is_eligible_for_bank_demographic, is_eligible_for_bureau_bank_demo, dedupe_status, application_stage=None):

    product = request["product"]
    partner = request["partner"]
    app_id = request["applicationId"]
    customer_id = request["customerId"]
    response["reasons"] += eligibility_reject_reasons

    risk_dict = {1: "A1", 2: "A2", 3: "A3", 4: "A4", 5: "A5"}
    try:
        app_score_band_int = int(app_score_band)
    except:
        app_score_band_int = 5
    try:
        co_app_score_band_int = int(co_app_score_band)
    except:
        co_app_score_band_int = 5
    app_score_band = risk_dict[app_score_band_int] if app_score_band_int in risk_dict else app_score_band_int
    co_app_score_band = risk_dict[co_app_score_band_int] if co_app_score_band_int in risk_dict else \
        co_app_score_band_int

    if is_applicant_ntc and str(app_score_band).upper() in ["A5", '5', 'S5', 'NONE']:
        app_score_band = 'NTC'

    if (is_co_applicant_ntc and is_co_applicant) and str(co_app_score_band).upper() in ["A5", '5', 'S5', 'NONE']:
        co_app_score_band = 'NTC'
    else:
        co_app_score_band = None if is_co_applicant is False else co_app_score_band

    if response.get("final_band") in ["S3", "S4"]:
        if app_co_app_qde_data.get("pol_any_property_own", "FAIL") in ["PASS", True]:
            pol_any_property_own = True
        else:
            pol_any_property_own = False
    else:
        pol_any_property_own = True

    if app_co_app_qde_data.get("pol_business_vintage") in ["RTC"] and response.get("final_band") in ["S5"]:
        pol_doctor_startup_s5 = True
    else:
        pol_doctor_startup_s5 = False

    if response["status"] in ["Accept"] and pol_eligibility_decision in ["PASS", "Accept"] and pol_bureau_score and pol_any_property_own:
        final_status = "Accept"
    elif response["status"] in ["Decline", "FAIL", "Reject"] or pol_eligibility_decision in ["Decline", "FAIL", "Reject"] or pol_bureau_score is False or pol_any_property_own in ["FAIL", False]:
        final_status = "Decline"

        if pol_bureau_score is False:
            response["reasons"] += ["pol_bureau_score is False"]

        if pol_any_property_own in ["FAIL", False]:
            response["reasons"] += ["pol_any_property_own is False"]

    elif response["status"] in ["Refer", "RTC"] or pol_eligibility_decision in ["Refer", "RTC"]:
        final_status = "Refer"
    else:
        final_status = response["status"]

    calculated_variables = {
        "dedupe_status": dedupe_status,
        "bureau_score": bureau_score,
        "pol_bureau_score": pol_bureau_score,
        "app_score_band": str(app_score_band),
        "guarantor_max_score_band": str(co_app_score_band),
        "is_eligible_for_only_bank_demographic": is_eligible_for_bank_demographic,
        "is_eligible_for_bureau_bank_demo": is_eligible_for_bureau_bank_demo,
        "banking_demographic_score_band": str(banking_demographic_score_band),
        "demographic_score_band": str(demographic_score_band),
        "asset_category": asset_type_cat,
        "asset_ltv": ltv_asset,
        "net_ltv": float("{:.3f}".format(ltv)),
        "gross_ltv": float("{:.3f}".format(gross_ltv)),
        "emi": round(final_abb_emi),
        "abb_to_emi": abb_to_emi,
        "final_abb_to_emi": common_var_dict.get("final_abb_to_emi"),
        "final_emi_loan_amount": round(final_emi_loan_amount),
        "initial_abb_loan_amount": round(initial_abb_loan_amount),
        "loan_amount_asset_ltv": round(loan_amount_asset_ltv),
        "sd_amount": common_var_dict.get("sd_amount"),
        "revised_sd_amount": common_var_dict.get("revised_sd_amount"),
        "debitable_abb": common_var_dict.get("debitable_abb"),
        "eligible_loan_amount(min)": common_var_dict.get("eligible_loan_amount(min)"),
        "final_loan_amount_net": common_var_dict.get("final_loan_amount_net"),
        "max_percentage_collateral": max_perc_collateral[0] if len(max_perc_collateral) > 0 else None,
        "applicant_bureau_resp": app_bureau_data,
        "guarantors_bureau_resp": co_applicant_data_list,
        "applicant_banking_resp": app_banking_data,
        "applicant_guarantors_qde_resp": app_co_app_qde_data,
        "final_roi": final_eligibility.get("final_roi"),
        "final_tenure": final_eligibility.get("final_tenure"),
        "final_loan_amount": final_eligibility.get("final_loan_amount"),
        "final_emi": final_eligibility.get("final_emi")
    }
    # calculated_variables.update(app_bureau_data)
    # calculated_variables.update(final_co_app_bureau_data)
    # calculated_variables.update(app_banking_data)
    # calculated_variables.update(app_co_app_qde_data)
    if final_status in ["Decline", "Reject"]:
        final_eligibility = {"final_roi": 0,
                             "final_tenure": 0,
                             "final_emi": 0,
                             "final_loan_amount": 0}

    resp_dict = {
            "status": "true",
            "loanApplicationId": app_id,
            "customerId": customer_id,
            "product": product,
            "partner": partner,
            "scorecard": {
                "scorecard_name": f"{os.environ['env']}:HFS_Scorecard",
                "scorecard_version": "1.0"
            },
            "bureau_response": {
                "calculated_variables": calculated_variables,
                "decision": {
                    "status": final_status,
                    "reasons": response["reasons"],
                    "final_band": response.get("final_band"),
                    "dedupe_status": dedupe_status,
                    "reject_review": False
                },
                "tradelines": [],
                "eligibility": final_eligibility,
                "extra_details": {
                    "document_list": [],
                    "applicationStage": application_stage
                }
            },
            "warnings": []

        }

    # query_dict = {"product": product, "partner": partner, "scorecard_version": "$LATEST", "loan_app_id": app_id,
    #               "customer_id": customer_id, "calculated_variables": json.dumps(response),
    #               "mobile_no": response["mobile_number"], "pan_no": pan,
    #               "status": response["pol_decision_status"],
    #               "reject_reasons": response["rejection_reason"],
    #               "eligibility": json.dumps(resp_dict["bureau_response"]["eligibility"][0]),
    #               "loanAmount": final_loan_amount,
    #               "extra_details": json.dumps(resp_dict["bureau_response"]["extra_details"]),
    #               "warnings": json.dumps(warnings)
    #               }
    #
    # lambda_client = boto3.client('lambda')
    # log_response = lambda_client.invoke(FunctionName="scorecard_async_logging", InvocationType='Event',
    #                                     Payload=json.dumps(query_dict))
    # log_resp = log_response['Payload'].read().decode()
    # logger.info("---------Scoreacrd logs Response---------:")
    # logger.info(log_resp)

    return resp_dict
