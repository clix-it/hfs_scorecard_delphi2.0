import json
import pandas as pd
import numpy as np
from dateutil import parser
from datetime import datetime
from functions.conf_functions import *
from HFS.rejection_reasons import *


def cleaner(row):
    disbursed_dt = date_cleaner(row['disbursed_dt'])
    sourcing_date = date_cleaner(row['sourcing_date'])
    overdue_amt = amt_cleaner(row['overdue_amt'])
    current_bal = amt_cleaner(row['current_bal'])
    suit_amt = amt_cleaner(row['suit_amt'])
    sanctioned_amt = amt_cleaner(row['sanctioned_amt'])
    dpd_list, nbr_reportings = dpd_cleaner(row['dpd_hist'])
    return pd.Series(
        [suit_amt, sanctioned_amt, current_bal, overdue_amt, disbursed_dt, sourcing_date,
         dpd_list, nbr_reportings],
        index=['suit_amt', 'sanctioned_amt', 'current_bal', 'overdue_amt', 'disbursed_dt',
               'sourcing_date', 'dpd_list', 'nbr_reportings'])


def derivations(row):
    months_since_open = open_since(row)
    suit_flag = derog_tagging(row)
    last_30plus_in_months = last_30plus_months_ago(row)
    nbr_30plus_l24m = count_30plus_l24m(row)
    max_delq_last3m = max_delinquent_in_period(row, 0, 92)
    max_delq_last6m = max_delinquent_in_period(row, 0, 184)
    max_delq_last12m = max_delinquent_in_period(row, 0, 365)
    max_delq_last6_12m = max_delinquent_in_period(row, 180, 365)
    max_delq_last12_36m = max_delinquent_in_period(row, 360, 1100)
    return pd.Series(
        [months_since_open, suit_flag, last_30plus_in_months, nbr_30plus_l24m, max_delq_last3m,
         max_delq_last6m,
         max_delq_last12m, max_delq_last6_12m, max_delq_last12_36m],
        index=['months_since_open', 'suit_flag', 'last_30plus_in_months', 'nbr_30plus_l24m',
               'max_delq_last3m', 'max_delq_last6m', 'max_delq_last12m', 'max_delq_last6_12m',
               'max_delq_last12_36m'])


def date_cleaner(data):
    dt = str(data).replace('/', '-')
    try:
        dt = parser.parse(dt, dayfirst=True)
    except:
        dt = None
    return dt #returns datatime
# =========================================================
# def date_cleaner(data):
#     """This function will clean string date to datetime format."""
#     if data is None or data in [""]:
#         return None
#     try:
#         dt = data.replace('/', '-').replace(' ', '').replace(',', '')
#         dt1 = datetime.strptime(dt, '%Y%m%d')
#         return dt1.strftime('%d-%m-%Y') #returns str
#     except Exception as E:
#         return None


def split_date_cleaner(data):
    """This function will clean string date to datetime format."""
    if data is None or data in [""]:
        return None
    try:
        dt = data.replace('/', '-').replace(' ', '').replace(',', '')
        dt1 = datetime.strptime(dt, '%d-%m-%Y')
        return dt1.strftime('%Y-%m-%d')
    except:
        return None


def amt_cleaner(data):
    """This function will clean string amount to float format."""
    try:
        data = str(data).replace('- ', '-')
        data = str(data).replace(',', '')
        data = data.strip()
        if data is None or data in ["", 'None', 'null']:
            return 0
        else:
            float_data = float(data)
            return float_data
    except:
        return 0


# ================================================================================


# def amt_cleaner(row):
#     str1 = str(row).replace(' ', '').replace(',', '')
#     try:
#         amt = float(str1)
#     except:
#         amt = 0
#     return amt


def open_since(row):
    try:
        months_since_open = round(
            (row['sourcing_date'] - pd.to_datetime(row['disbursed_dt'])).days / 365.0 * 12.0)
    except Exception as e:
        months_since_open = 0
    return months_since_open


def dpd_cleaner(dpd_list):  #[30/Jun 2017, 60/MAy 2018]
    dpd_string_list = []
    for dpd_string in dpd_list:

        dpd = str(dpd_string).split('/')[0]
        dpd_date = str(dpd_string).split('/')[1]
        dpd_date = str(dpd_date).strip().replace(" ", "-")
        dpd = str(dpd).strip().replace(' Days Past Due', '').replace('XXX', '0').replace(" ", "")
        try:
            if dpd in ["", "None", "NONE", "Standard", "0 Day Past Due", "0"]:
                new_dpd = 0
            elif dpd in ["Sub-standard", "Doubtful", "Loss", "999", "above Days Past Due", "above"
                         "999 or above Days Past Due", "DOUBTFUL-1", "DOUBTFUL-2", "DOUBTFUL-3", "Non Performing Assets",
                         "ARC Loans"] or int(dpd) >= 90:
                new_dpd = 90
            elif dpd in ["1 Day Past Due", "1"]:
                new_dpd = 1
            elif dpd in ["2 Days Past Due", "2"]:
                new_dpd = 2
            elif 'SMA-0' in dpd or 0 < int(dpd) < 30:
                new_dpd = 29
            elif 'SMA-1' in dpd or 30 < int(dpd) < 60:
                new_dpd = 30
            elif dpd in ["Special Mention Accounts", "SMA", "SMA-2"] or 60 < int(dpd) < 90:
                new_dpd = 60
            else:
                clean_dpd = str(dpd).strip().replace(' Days Past Due', '').replace('XXX', '0').replace(" ", "")
                try:
                    new_dpd = int(clean_dpd)
                except:
                    new_dpd = 0
        except:
            clean_dpd = str(dpd).strip().replace(' Days Past Due', '').replace('XXX', '0').replace(" ", "")
            try:
                new_dpd = int(clean_dpd)
            except:
                new_dpd = 0
        dpd_string_list.append(f"{new_dpd}/{dpd_date}")

    return dpd_string_list


def derog_tagging(row):
    try:
        suit_amt = int(str(row['suit_amt']).replace(",", ""))
    except:
        suit_amt = 0

    if str(row['wilful_defaulter']) in ['Wilful Defaulter']:
        suit_flag = 1
    elif str(row['suit_filed_status']) in ['Suit Filed Case', 'Suit Filed']:
        suit_flag = 1
    elif suit_amt > 1000:
        suit_flag = 1
    else:
        suit_flag = 0
    return suit_flag


def last_30plus_months_ago(row):
    delq_dict = {'XXX': 0, '000': 0, 'STD': 0, 'SM0': 0, 'SUB': 90, 'DBT': 180, 'LOS': 180,
                 'LSS': 180, 'SMA': 60,
                 'SM1': 60, 'SM2': 90, 'DDD': 0, '?': 0, 'nan': 0, 'L01': 0, 'L02': 30, 'L03': 60,
                 'L04': 90,
                 'L05': 180}
    last_30plus_in_months = None
    try:
        for dpd_string in row['dpd_with_month']:  #
            dpd = str(dpd_string).split('/')[0]
            if int(dpd) >= 30:
                last_30plus_in_months = round(
                    (row['sourcing_date'] - parser.parse(
                        '01-' + str(dpd_string).split('/')[1])).days / 365.0 * 12.0)
                return last_30plus_in_months
    except:
        return last_30plus_in_months

    return last_30plus_in_months


def count_30plus_l24m(row):
    delq_dict = {'XXX': 0, '000': 0, 'STD': 0, 'SM0': 0, 'SUB': 90, 'DBT': 180, 'LOS': 180,
                 'LSS': 180, 'SMA': 60,
                 'SM1': 60, 'SM2': 90, 'DDD': 0, '?': 0, 'nan': 0, 'L01': 0, 'L02': 30, 'L03': 60,
                 'L04': 90,
                 'L05': 180}
    nbr_30plus_l24m = 0
    for dpd_string in row['dpd_with_month']:
        if (row['sourcing_date'] - parser.parse('01-' + str(dpd_string).split('/')[1])).days < 732:
            dpd = str(dpd_string).split('/')[0]
            if int(dpd) >= 30:
                nbr_30plus_l24m = nbr_30plus_l24m + 1
    return nbr_30plus_l24m


def max_delinquent_in_period(row, start_dys, end_dys):
    delq_dict = {'XXX': 0, '000': 0, 'STD': 0, 'SM0': 0, 'SUB': 90, 'DBT': 180, 'LOS': 180,
                 'LSS': 180, 'SMA': 60,
                 'SM1': 60, 'SM2': 90, 'DDD': 0, '?': 0, 'nan': 0, 'L01': 0, 'L02': 30, 'L03': 60,
                 'L04': 90,
                 'L05': 180}
    max_delq = 0
    for dpd_string in row['dpd_with_month']:
        if (((row['sourcing_date'] - parser.parse(
                '01-' + str(dpd_string).split('/')[1])).days > start_dys) & (
                (row['sourcing_date'] - parser.parse(
                    '01-' + str(dpd_string).split('/')[1])).days < end_dys)):
            max_delq = max(int(str(dpd_string).split('/')[0]), max_delq)
    return max_delq


def calculated_varibles(tradelines):
    al_max_pct_paid_wc_list = []
    max_tl_disb_amt_l36m_list = []
    tot_cb_amt_list = []
    tot_od_amt_list = []
    tot_sc_amt_list = []
    monsincel30p_tl_l36m_list = []
    no_of_30dpd_el_l24m_list = []
    nbr_el_accs_list = []
    max_pct_paid_el_list = []
    max_prev_overdue_tl_list = []
    max_pct_paid_tl_list = []
    max_wc_disb_amt_l36m_list = []
    suit_flag_list = []
    nbr_1plus_3m_list = []
    nbr_30plus_6m_list = []
    nbr_60plus_12m_list = []
    nbr_90plus_12m_list = []
    nbr_30plus_6_12m_list = []
    nbr_90plus_12_36m_list = []
    bureau_score_list = []
    nbr_tradelines_list = []
    max_mob_ovral_list = []
    no_derog_36mths_list = []
    pol_90_plus_36mth_list = []
    pol_60_plus_24mth_list = []
    pol_30_plus_12mth_list = []
    pol_0_plus_6mth_list = []
    pol_only_gold_loan_list = []
    acct_type_name_list1 = ["Demand Loan", "Cash Credit", "Overdraft",
                            "Packing credit (all export pre-shipment finance)",
                            "Inland bills discounted", "Export bills purchased",
                            "Export bills dicounted", "Inland bills purchased",
                            "Advances against import bills",
                            "Loan Extended through Credit Card",
                            "Export bills advanced against",
                            "Corporate credit card",
                            "Advances against export cash incentives and duty draw-back claims"]

    acct_type_name_list2 = ["Medium Term Loan (Period above 1 year and upto 3 years)",
                            "Long term loan (period above 3 years)",
                            "Auto Loan", "Short term loan (less than 1 year)",
                            "Corporate vechile loan",
                            "Equipment financing (construction office medical)",
                            "AutoLoan(Personal)",
                            "Hire Purchase",
                            "Unsecured business loan", "CommercialVehicleLoan",
                            "Property Loan",
                            "PropertyLoan",
                            "ConstructionEquipmentLoan", "Lease finance", "UsedCarLoan",
                            "TractorLoan"]

    for row in tradelines:
        al_max_pct_paid_wc_list.append(row['pct_paid'] if (row['nbr_reportings'] > 4) and
                                                          (row['active_flag'] == 1) and
                                                          row['acct_type_name'] in acct_type_name_list1
                                       else 0)
        max_tl_disb_amt_l36m_list.append(row['sanctioned_amt'] if row['nbr_reportings'] > 4 and
                                                                  row['months_since_open'] <= 36 and
                                                                  row['acct_type_name'] in acct_type_name_list2 else 0)

        tot_cb_amt_list.append(row['current_bal'] if row['nbr_reportings'] > 4 else 0)
        tot_od_amt_list.append(row['overdue_amt'] if row['nbr_reportings'] > 4 else 0)
        tot_sc_amt_list.append(row['sanctioned_amt'] if row['nbr_reportings'] > 4 else 0)
        monsincel30p_tl_l36m_list.append(row['last_30plus_in_months'] if row['nbr_reportings'] > 4 and
                                                                         row['acct_type_name'] in acct_type_name_list2 else None)
        no_of_30dpd_el_l24m_list.append(row['nbr_30plus_l24m'] if row['nbr_reportings'] > 4
                                                                  and
                                                                  row['acct_type_name'] == 'Equipment financing (construction office medical)'
                                        else 0)
        nbr_el_accs_list.append(1 if row['nbr_reportings'] > 4 and
                                     row['acct_type_name'] == 'Equipment financing (construction office medical)' else 0)
        max_pct_paid_el_list.append(row['pct_paid'] if row['nbr_reportings'] > 4 and
                                                       row['acct_type_name'] == 'Equipment financing (construction office medical)' else 0)
        max_prev_overdue_tl_list.append(row['overdue_amt'] if row['nbr_reportings'] > 4 and \
                                                              row['acct_type_name'] in
                                                              acct_type_name_list2 else 0)
        max_pct_paid_tl_list.append(row['pct_paid'] if row['nbr_reportings'] > 4 and \
                                                       row['acct_type_name'] in acct_type_name_list2
                                    else 0)
        max_wc_disb_amt_l36m_list.append(row['sanctioned_amt'] if row['nbr_reportings'] > 4 and
                                                                  row['months_since_open'] <= 36 and
                                                                  row['acct_type_name'] in acct_type_name_list1 else 0)

        suit_flag_list.append(row['suit_flag'])
        nbr_1plus_3m_list.append(1 if row['max_delq_last3m'] >= 1 else 0)
        nbr_30plus_6m_list.append(1 if row['max_delq_last6m'] >= 30 else 0)
        nbr_60plus_12m_list.append(1 if row['max_delq_last12m'] >= 60 else 0)
        nbr_90plus_12m_list.append(1 if row['max_delq_last12m'] >= 90 else 0 )
        nbr_30plus_6_12m_list.append(1 if row['max_delq_last6_12m'] >= 30 else 0)
        nbr_90plus_12_36m_list.append(1 if row['max_delq_last12_36m'] >= 90 else 0)
        bureau_score_list.append(row['crif_score'])
        nbr_tradelines_list.append(row['tradeline_no'])
        max_mob_ovral_list.append(row['months_since_open'])

        no_derog_36mths_list.append(row["no_derog_36mths"])
        pol_90_plus_36mth_list.append(row["pol_90_plus_36mth"])
        pol_60_plus_24mth_list.append(row["pol_60_plus_24mth"])
        pol_30_plus_12mth_list.append(row["pol_30_plus_12mth"])
        pol_0_plus_6mth_list.append(row["pol_0_plus_6mth"])
        pol_only_gold_loan_list.append(row["pol_only_gold_loan"])

    return al_max_pct_paid_wc_list, max_tl_disb_amt_l36m_list, tot_cb_amt_list, \
        tot_od_amt_list, tot_sc_amt_list, monsincel30p_tl_l36m_list, \
        no_of_30dpd_el_l24m_list,  nbr_el_accs_list, max_pct_paid_el_list, \
        max_prev_overdue_tl_list, max_pct_paid_tl_list, max_wc_disb_amt_l36m_list, suit_flag_list, \
        nbr_1plus_3m_list, nbr_30plus_6m_list, nbr_60plus_12m_list, \
        nbr_90plus_12m_list, nbr_30plus_6_12m_list, nbr_90plus_12_36m_list, \
        bureau_score_list, nbr_tradelines_list, max_mob_ovral_list, no_derog_36mths_list, pol_90_plus_36mth_list, \
           pol_60_plus_24mth_list, pol_30_plus_12mth_list, pol_0_plus_6mth_list, pol_only_gold_loan_list


def hfs_commercial(commercial_bureau):
    sourcing_date = commercial_bureau.sourcing_date
    try:
        request_date = datetime.strptime(sourcing_date, '%d-%b-%Y')
    except:
        request_date = datetime.strptime(sourcing_date, '%d-%m-%Y')
    request_id = commercial_bureau.request_id
    los_app_id = commercial_bureau.los_app_id
    crif_score = commercial_bureau.crif_score
    tradelines = commercial_bureau.tradelines
    num_of_tradelines = commercial_bureau.num_of_tradeline
    inquiry = commercial_bureau.enquiry_tradeline

    tradelines = del_gold_loan_agri_loans("CIBIL", tradelines)

    no_hit = check_no_hit_with_months_hfs(request_date, tradelines, months=6)

    active_flag = lambda x: 1 if ('ACTIVE' in x or 'Open' in x) else 0
    pct_paid = lambda x: (1.0 - (x['current_bal'] * 1.0 / x['sanctioned_amt'])) * 100.0 if (
            x['sanctioned_amt'] > 0) else 0
    type_of_xml = "CIBIL"

    if int(num_of_tradelines) > 0 and not no_hit:
        for row in tradelines:
            row['crif_score'] = crif_score
            row['request_id'] = request_id
            row['los_app_id'] = los_app_id
            row['sourcing_date'] = date_cleaner(sourcing_date)
            row['disbursed_dt'] = date_cleaner(row['disbursed_dt'])
            row['suit_amt'] = amt_cleaner(row['suit_amt'])
            row['current_bal'] = amt_cleaner(row['current_bal'])
            row['overdue_amt'] = amt_cleaner(row['overdue_amt'])
            row['dpd_with_month'] = dpd_cleaner(row['dpd_hist'])
            row['sanctioned_amt'] = amt_cleaner(row['sanctioned_amt'])
            row['active_flag'] = active_flag(row['credit_status'])
            row['pct_paid'] = pct_paid(row)
            row_mob = open_since(row)
            row['months_since_open'] = row_mob
            row["mob_source_date"] = row_mob
            row["mob"] = row_mob
            row['suit_flag'] = derog_tagging(row)
            row['last_30plus_in_months'] = last_30plus_months_ago(row)
            row['nbr_30plus_l24m'] = count_30plus_l24m(row)
            row['max_delq_last3m'] = max_delinquent_in_period(row, 0, 92)
            row['max_delq_last6m'] = max_delinquent_in_period(row, 0, 184)
            row['max_delq_last12m'] = max_delinquent_in_period(row, 0, 365)
            row['max_delq_last6_12m'] = max_delinquent_in_period(row, 180, 365)
            row['max_delq_last12_36m'] = max_delinquent_in_period(row, 360, 1100)

            row["no_derog_36mths"] = no_derog_36mths_fun_hfs(row, type_of_xml)
            row["pol_90_plus_36mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, request_date, 90, 36, type_of_xml)
            row["pol_60_plus_24mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, request_date, 60, 24, type_of_xml)
            row["pol_30_plus_12mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, request_date, 30, 12, type_of_xml)
            row["pol_0_plus_6mth"] = pol_0_plus_current_fun_hfs(row, request_date, 0, 6, type_of_xml)
            row["pol_only_gold_loan"] = pol_only_gold_loan_func_hfs(row, type_of_xml)
            row["pol_bl_loan"] = pol_bl_loan_func_hfs(row, type_of_xml)
        result = hfs_commercial_scorecard(tradelines, request_id, inquiry)
        return result
    else:
        if num_of_tradelines > 0:
            if no_hit:
                rejection_reason = ['no_hit is True (NTC)']
            else:
                rejection_reason = ["no_hit is False"]
        else:
            rejection_reason = ['No Tradelines in Bureau']

        return {
            'bureau_type': "Commercial",
            'request_id': request_id,
            'al_max_pct_paid_wc': None,
            'max_tl_disb_amt_l36m': None,
            'pct_tot_ovd_bal': None,
            'monsincel30p_tl_l36m': None,
            'nbr_el_accs': None,
            'no_of_30dpd_el_l24m': None,
            'max_pct_paid_el': None,
            'max_prev_overdue_tl': None,
            'pct_tot_bal_hicr': None,
            'max_pct_paid_tl': None,
            'max_wc_disb_amt_l36m': None,
            'al_max_pct_paid_wc_score': None,
            'max_tl_disb_amt_l36m_score': None,
            'pct_tot_ovd_bal_score': None,
            'monsincel30p_tl_l36m_score': None,
            'no_of_30dpd_el_l24m_score': None,
            'max_pct_paid_el_score': None,
            'max_prev_overdue_tl_score': None,
            'pct_tot_bal_hicr_score': None,
            'max_pct_paid_tl_score': None,
            'max_wc_disb_amt_l36m_score': None,
            'final_commercial_cb_score': None,
            'commercial_cb_score_seg': None,
            'nbr_suit_filed': None,
            "max_mob_ovral": None,
            "nbr_tradelines": num_of_tradelines,
            'bureau_score': crif_score,
            "crif_score": crif_score,
            "pol_no_derog_36mths": None,
            "pol_no_90_plus_36mths": None,
            "pol_no_60_plus_24mths": None,
            "pol_no_30_plus_12mths": None,
            "pol_no_0_plus_6mth": None,
            "pol_only_gold_loan": None,
            "pol_no_6_unsecured_enq_3mth": None,
            "pol_bureau_status": None,
            'hit_status': no_hit,
            'no_hit': no_hit,
            'pol_decision_status': "Decline",
            'rejection_reason': rejection_reason,
            "commercial_tradelines": []
        }


def hfs_commercial_scorecard(tradelines,request_id, inquiry):
    al_max_pct_paid_wc_list, max_tl_disb_amt_l36m_list, tot_cb_amt_list, \
        tot_od_amt_list, tot_sc_amt_list, monsincel30p_tl_l36m_list, no_of_30dpd_el_l24m_list, \
        nbr_el_accs_list, max_pct_paid_el_list, max_prev_overdue_tl_list, \
        max_pct_paid_tl_list, max_wc_disb_amt_l36m_list, suit_flag_list, \
        nbr_1plus_3m_list, nbr_30plus_6m_list, nbr_60plus_12m_list, \
        nbr_90plus_12m_list, nbr_30plus_6_12m_list, nbr_90plus_12_36m_list, \
        bureau_score_list, nbr_tradelines_list, max_mob_ovral_list, no_derog_36mths_list, pol_90_plus_36mth_list, \
    pol_60_plus_24mth_list, pol_30_plus_12mth_list, pol_0_plus_6mth_list, pol_only_gold_loan_list = calculated_varibles(tradelines)

    al_max_pct_paid_wc = max(al_max_pct_paid_wc_list)

    if al_max_pct_paid_wc <= 10.0:
        al_max_pct_paid_wc_score = 63
    elif al_max_pct_paid_wc <= 52.0:
        al_max_pct_paid_wc_score = 79
    elif al_max_pct_paid_wc > 52.0:
        al_max_pct_paid_wc_score = 102
    else:
        al_max_pct_paid_wc_score = 63

    max_tl_disb_amt_l36m = max(max_tl_disb_amt_l36m_list)
    if 0 < max_tl_disb_amt_l36m <= 3000000:
        max_tl_disb_amt_l36m_score = 64
    elif max_tl_disb_amt_l36m > 3000000:
        max_tl_disb_amt_l36m_score = 81
    else:
        max_tl_disb_amt_l36m_score = 59

    tot_cb_amt = sum(tot_cb_amt_list)
    tot_od_amt = sum(tot_od_amt_list)
    tot_sc_amt = sum(tot_sc_amt_list)

    if tot_cb_amt > 0:
        pct_tot_ovd_bal = float("{0:.4f}".format(tot_od_amt * 100.0 / tot_cb_amt))
    else:
        pct_tot_ovd_bal = None

    if pct_tot_ovd_bal is not None:
        if pct_tot_ovd_bal == 0:
            pct_tot_ovd_bal_score = 92
        elif 0 < pct_tot_ovd_bal <= 0.0021:
            pct_tot_ovd_bal_score = 73
        elif pct_tot_ovd_bal > 0.0021:
            pct_tot_ovd_bal_score = 33
        else:
            pct_tot_ovd_bal_score = 100
    else:
        pct_tot_ovd_bal_score = 0

    # MONSINCEL30P_TL_L36M

    try:
        monsincel30p_tl_l36m = min([month for month in monsincel30p_tl_l36m_list if month is not None])
    except:
        monsincel30p_tl_l36m = 0

    if monsincel30p_tl_l36m <= 10:
        monsincel30p_tl_l36m_score = 14
    elif monsincel30p_tl_l36m > 10:
        monsincel30p_tl_l36m_score = 78
    else:
        monsincel30p_tl_l36m_score = 92

    # if monsincel30p_tl_l36m > 36:
    #     monsincel30p_tl_l36m = 0

    # NO_OF_30DPD_EL_L24M

    nbr_el_accs = sum(nbr_el_accs_list)
    if ((nbr_el_accs == 0)):
        no_of_30dpd_el_l24m = 0
    else:
        no_of_30dpd_el_l24m = sum(no_of_30dpd_el_l24m_list)

    if no_of_30dpd_el_l24m == 0:
        no_of_30dpd_el_l24m_score = 81
    elif no_of_30dpd_el_l24m > 0:
        no_of_30dpd_el_l24m_score = 51
    else:
        no_of_30dpd_el_l24m_score = 58
    # max_pct_paid_el
    max_pct_paid_el = max(max_pct_paid_el_list)
    if max_pct_paid_el <= 33.0:
        max_pct_paid_el_score = 61
    elif max_pct_paid_el <= 90.0:
        max_pct_paid_el_score = 73
    elif max_pct_paid_el > 90.0:
        max_pct_paid_el_score = 90
    else:
        max_pct_paid_el_score = 63

    # max_prev_overdue_tl
    max_prev_overdue_tl = max(max_prev_overdue_tl_list)
    if max_prev_overdue_tl == 0:
        max_prev_overdue_tl_score = 80
    elif max_prev_overdue_tl <= 60000:
        max_prev_overdue_tl_score = 76
    elif max_prev_overdue_tl > 60000:
        max_prev_overdue_tl_score = 62
    else:
        max_prev_overdue_tl_score = 78
    # pct_tot_bal_hicr
    if tot_sc_amt > 0:
        pct_tot_bal_hicr = float("{0:.4f}".format(tot_cb_amt * 100.0 / tot_sc_amt))
    else:
        pct_tot_bal_hicr = 0

    if 0 < pct_tot_bal_hicr <= 15.0:
        pct_tot_bal_hicr_score = 96
    elif pct_tot_bal_hicr <= 76.0:
        pct_tot_bal_hicr_score = 73
    elif pct_tot_bal_hicr > 76.0:
        pct_tot_bal_hicr_score = 60
    else:
        pct_tot_bal_hicr_score = 120

    # max_pct_paid_tl
    max_pct_paid_tl = max(max_pct_paid_tl_list)

    if max_pct_paid_tl <= 43.0:
        max_pct_paid_tl_score = 54
    elif max_pct_paid_tl > 43.0:
        max_pct_paid_tl_score = 79
    else:
        max_pct_paid_tl_score = 85

    # max_wc_disb_amt_l36m
    max_wc_disb_amt_l36m = max(max_wc_disb_amt_l36m_list)

    if 0 < max_wc_disb_amt_l36m <= 3500000:
        max_wc_disb_amt_l36m_score = 66
    elif max_wc_disb_amt_l36m > 3500000:
        max_wc_disb_amt_l36m_score = 101
    else:
        max_wc_disb_amt_l36m_score = 56

    final_commercial_cb_score = al_max_pct_paid_wc_score + max_tl_disb_amt_l36m_score + \
        pct_tot_ovd_bal_score + monsincel30p_tl_l36m_score + no_of_30dpd_el_l24m_score + \
        max_pct_paid_el_score + max_prev_overdue_tl_score + pct_tot_bal_hicr_score + \
        max_pct_paid_tl_score + max_wc_disb_amt_l36m_score

    if final_commercial_cb_score > 706:
        commercial_cb_score_seg = 1      # Low Risk
    elif 685 < final_commercial_cb_score <= 706:
        commercial_cb_score_seg = 2
    elif 671 < final_commercial_cb_score >= 685:
        commercial_cb_score_seg = 3
    elif 635 < final_commercial_cb_score >= 671:
        commercial_cb_score_seg = 4
    else:
        commercial_cb_score_seg = 5      # High Risk

    nbr_suit_filed = sum(suit_flag_list)
    nbr_1plus_3m = sum(nbr_1plus_3m_list)
    nbr_30plus_6m = sum(nbr_30plus_6m_list)
    nbr_60plus_12m = sum(nbr_60plus_12m_list)
    nbr_90plus_12m = sum(nbr_90plus_12m_list)
    nbr_30plus_6_12m = sum(nbr_30plus_6_12m_list)
    nbr_90plus_12_36m = sum(nbr_90plus_12_36m_list)
    bureau_score = max(bureau_score_list)
    nbr_tradelines = max(nbr_tradelines_list)
    max_mob_ovral = max(max_mob_ovral_list)

    if nbr_tradelines == 0 or max_mob_ovral <= 6:
        hit_status = True
    else:
        hit_status = False

    if hit_status:
        commercial_cb_score_seg = 'Not Scored'
        final_commercial_cb_score = 0

    try:
        request_date = datetime.strptime(tradelines[0]["sourcing_date"], '%d-%b-%Y')
    except:
        request_date = tradelines[0]["sourcing_date"]

    inquiry_dates = [x['enquiryDt'] for x in inquiry if x["enquiryPurpose"] in ["Others", "Unsecured business loan", "Business Loan - Unsecured"]]

    nbr_unsecured_enquiry_1_mth = int(inq_date_fun(inquiry_dates, request_date, 1))
    nbr_unsecured_enquiry_3_mth = int(inq_date_fun(inquiry_dates, request_date, 3))

    if nbr_unsecured_enquiry_3_mth <= 6:
        pol_no_6_unsecured_enq_3mth = True
    else:
        pol_no_6_unsecured_enq_3mth = False

    try:
        crif_score = int(tradelines[0]["crif_score"])
    except:
        crif_score = None

    type_of_xml = "CIBIL"

    pol_only_gold_loan = all(pol_only_gold_loan_list) if len(pol_only_gold_loan_list) > 0 else False

    pol_no_derog_36mths = False if int(sum(no_derog_36mths_list)) > 0 else True

    pol_no_90_plus_36mths = False if int(sum(pol_90_plus_36mth_list)) > 0 else True

    pol_no_60_plus_24mths = False if int(sum(pol_60_plus_24mth_list)) > 0 else True

    pol_no_30_plus_12mths = False if int(sum(pol_30_plus_12mth_list)) > 0 else True
    pol_no_0_plus_6mth = False if int(sum(pol_0_plus_6mth_list)) > 0 else True

    try:
        if hit_status is False and crif_score is not None and 0 < crif_score < 7:
            pol_bureau_status = True
        else:
            pol_bureau_status = False
    except:
        pol_bureau_status = True

    if pol_no_derog_36mths is True and \
            pol_no_90_plus_36mths is True and \
            pol_no_60_plus_24mths is True and \
            pol_no_30_plus_12mths is True and \
            pol_no_0_plus_6mth is True:
        if pol_no_6_unsecured_enq_3mth is False:
            pol_decision_status = "Refer"
        else:
            pol_decision_status = "Accept"
    else:
        pol_decision_status = "Decline"

    if pol_decision_status in ["Decline"]:
        rejection_reason = commercial_rejection_reasons(pol_bureau_status, bureau_score, pol_no_derog_36mths, pol_no_90_plus_36mths,
                                                        pol_no_60_plus_24mths, pol_no_30_plus_12mths, pol_no_0_plus_6mth,
                                                        pol_no_6_unsecured_enq_3mth)
    else:
        rejection_reason = []

    commercial_tradelines = []
    for row in tradelines:
        each_trad = {}
        each_trad["mob"] = row["mob"]
        each_trad["pct_paid"] = row["pct_paid"]
        each_trad["acct_type_name"] = row["acct_type_name"]
        each_trad["current_bal"] = row["current_bal"]
        each_trad["sanctioned_amt"] = row["sanctioned_amt"]
        each_trad["nbr_reportings"] = row["nbr_reportings"]
        each_trad["active_flag"] = row["active_flag"]
        each_trad["months_since_open"] = row["months_since_open"]
        each_trad["overdue_amt"] = row["overdue_amt"]
        each_trad["suit_flag"] = row["suit_flag"]
        each_trad["no_derog_36mths"] = row["no_derog_36mths"]
        each_trad["pol_90_plus_36mth"] = row["pol_90_plus_36mth"]
        each_trad["pol_60_plus_24mth"] = row["pol_60_plus_24mth"]
        each_trad["pol_30_plus_12mth"] = row["pol_30_plus_12mth"]
        each_trad["pol_0_plus_6mth"] = row["pol_0_plus_6mth"]
        each_trad["pol_only_gold_loan"] = row["pol_only_gold_loan"]
        each_trad["pol_bl_loan"] = row["pol_bl_loan"]
        commercial_tradelines.append(each_trad)

    calculated_varibles_dict = {
        'request_id': request_id,
        'bureau_type': "Commercial",
        'al_max_pct_paid_wc': al_max_pct_paid_wc,
        'max_tl_disb_amt_l36m': max_tl_disb_amt_l36m,
        'pct_tot_ovd_bal': None if pct_tot_ovd_bal is None else pct_tot_ovd_bal,
        'monsincel30p_tl_l36m': monsincel30p_tl_l36m,
        'nbr_el_accs': nbr_el_accs,
        'no_of_30dpd_el_l24m': no_of_30dpd_el_l24m,
        'max_pct_paid_el': max_pct_paid_el,
        'max_prev_overdue_tl': max_prev_overdue_tl,
        'pct_tot_bal_hicr': float("{0:.4f}".format(pct_tot_bal_hicr)),
        'max_pct_paid_tl': max_pct_paid_tl,
        'max_wc_disb_amt_l36m': max_wc_disb_amt_l36m,
        'al_max_pct_paid_wc_score': al_max_pct_paid_wc_score,
        'max_tl_disb_amt_l36m_score': max_tl_disb_amt_l36m_score,
        'pct_tot_ovd_bal_score': pct_tot_ovd_bal_score,
        'monsincel30p_tl_l36m_score': monsincel30p_tl_l36m_score,
        'no_of_30dpd_el_l24m_score': no_of_30dpd_el_l24m_score,
        'max_pct_paid_el_score': max_pct_paid_el_score,
        'max_prev_overdue_tl_score': max_prev_overdue_tl_score,
        'pct_tot_bal_hicr_score': pct_tot_bal_hicr_score,
        'max_pct_paid_tl_score': max_pct_paid_tl_score,
        'max_wc_disb_amt_l36m_score': max_wc_disb_amt_l36m_score,
        'final_commercial_cb_score': final_commercial_cb_score,
        'commercial_cb_score_seg': commercial_cb_score_seg,
        'nbr_suit_filed': nbr_suit_filed,
        "max_mob_ovral": max_mob_ovral,
        "nbr_tradelines": nbr_tradelines,
        'bureau_score': bureau_score,
        "crif_score": crif_score,
        "pol_no_derog_36mths": pol_no_derog_36mths,
        "pol_no_90_plus_36mths": pol_no_90_plus_36mths,
        "pol_no_60_plus_24mths": pol_no_60_plus_24mths,
        "pol_no_30_plus_12mths": pol_no_30_plus_12mths,
        "pol_no_0_plus_6mth": pol_no_0_plus_6mth,
        "pol_only_gold_loan": pol_only_gold_loan,
        "pol_no_6_unsecured_enq_3mth": pol_no_6_unsecured_enq_3mth,
        "pol_bureau_status": pol_bureau_status,
        'hit_status': hit_status,
        "no_hit": hit_status,
        'pol_decision_status': pol_decision_status,
        'rejection_reason': rejection_reason,
        "commercial_tradelines": commercial_tradelines
    }
    return calculated_varibles_dict
