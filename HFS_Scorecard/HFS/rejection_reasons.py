def rejection_reasons(bureau_status, bureau_score, pol_no_derog_36mths, pol_no_90_plus_36mths,
                      pol_no_60_plus_24mths, pol_no_30_plus_12mths, pol_no_0_plus_6mth,
                      pol_no_6_unsecured_enq_3mth):
    reasons = []

    # if bureau_status not in [True]:
    #     reasons.append('bureau_status is False as bureau score is {}'.format(bureau_score))

    if pol_no_derog_36mths not in [True]:
        reasons.append('pol_no_derog_36mths is False')

    if pol_no_60_plus_24mths not in [True]:
        reasons.append('pol_no_60_plus_24mths is False')

    if pol_no_30_plus_12mths not in [True]:
        reasons.append('pol_no_30_plus_12mths is False')

    if pol_no_0_plus_6mth not in [True]:
        reasons.append('pol_no_0_plus_6mth is False')

    if pol_no_6_unsecured_enq_3mth not in [True]:
        reasons.append('pol_no_6_unsecured_enq_3mth is False')

    if pol_no_90_plus_36mths not in [True]:
        reasons.append('pol_no_90_plus_36mths is False')

    return reasons


def commercial_rejection_reasons(bureau_status, bureau_score, pol_no_derog_36mths, pol_no_90_plus_36mths,
                                 pol_no_60_plus_24mths, pol_no_30_plus_12mths, pol_no_0_plus_6mth,
                                 pol_no_6_unsecured_enq_3mth):
    reasons = []
    # if bureau_status not in [True]:
    #     reasons.append('commercial_bureau_status is False as bureau score is {}'.format(bureau_score))

    if pol_no_derog_36mths not in [True]:
        reasons.append('commercial_pol_no_derog_36mths is False')

    if pol_no_60_plus_24mths not in [True]:
        reasons.append('commercial_pol_no_60_plus_24mths is False')

    if pol_no_30_plus_12mths not in [True]:
        reasons.append('commercial_pol_no_30_plus_12mths is False')

    if pol_no_0_plus_6mth not in [True]:
        reasons.append('commercial_pol_no_0_plus_6mth is False')

    if pol_no_6_unsecured_enq_3mth not in [True]:
        reasons.append('commercial_pol_no_6_unsecured_enq_3mth is False')

    if pol_no_90_plus_36mths not in [True]:
        reasons.append('commercial_pol_no_90_plus_36mths is False')

    return reasons


def bank_rejection_reasons(pol_perc_inward_bounce, pol_perc_outward_bounce,
                           pol_credits_bt, pol_bank_details_gt_6_mnth, is_all_bank_last_6_mnth_details,
                           pol_emi_bounces, pol_name_match, pol_doctor_startup, is_doctor_startup_override):
    reasons = []

    if pol_perc_inward_bounce not in [True, "PASS"]:
        reasons.append("pol_perc_inward_bounce is False")

    if pol_perc_outward_bounce not in [True, "PASS"]:
        reasons.append("pol_perc_outward_bounce is False")

    if pol_credits_bt not in [True, "PASS"]:
        reasons.append("pol_credits_bt is False")

    # if pol_num_banks not in [True, "PASS"]:
    #     reasons.append("Bank Accounts more than 3")

    if pol_bank_details_gt_6_mnth not in [True, "PASS"]:
        reasons.append("pol_bank_details_gt_6_mnth is False")

    if is_all_bank_last_6_mnth_details not in [True, "PASS"]:
        reasons.append("all_bank_last_6_monthly_details is False")

    if pol_emi_bounces not in [True, "PASS"]:
        reasons.append("pol_emi_bounces is False")

    if pol_name_match not in [True, "PASS"]:
        reasons.append("Banking refer due to name match")

    if pol_doctor_startup in ["RTC"] and is_doctor_startup_override:
        reasons.append("Banking rules override due to doctor startup.")

    return reasons


def qde_rejection_reasons(pol_qualification_experience, pol_qualification, pol_business_vintage, pol_age_match,
                          pol_residence_match, pol_app_office_match,
                          pol_refurbished_asset_type, pol_app_negative_area_match, pol_total_asset_value):
    reasons = []

    if pol_qualification_experience not in [True, "PASS"]:
        reasons.append(f"pol_qualification_experience is {pol_qualification_experience}")

    if pol_qualification not in [True, "PASS"]:
        reasons.append(f"pol_qualification is {pol_qualification}")

    if pol_business_vintage not in [True, "PASS"]:
        reasons.append(f"applicant business vintage is {pol_business_vintage}")

    if pol_age_match not in [True, "PASS"]:
        reasons.append("Applicant age not in range")

    if pol_residence_match not in [True, "PASS"]:
        reasons.append(f"pol_app_residence_match is {pol_residence_match}")

    if pol_refurbished_asset_type not in [True, "PASS"]:
        reasons.append(f"pol_refurbished_asset_type is {pol_refurbished_asset_type}")

    if pol_app_office_match not in [True, "PASS"]:
        reasons.append(f"pol_app_office_match is {pol_app_office_match}")

    if pol_app_negative_area_match not in [True, "PASS"]:
        reasons.append("Applicant living in a location where defaults are high")

    if pol_total_asset_value not in [True, "PASS"]:
        reasons.append("total asset value is 0")

    return reasons
