import pandas as pd
from datetime import date, datetime
from HFS.rejection_reasons import *


def age_calculator(birth_date):
    """
    input params:
        birth_date (str): date of birth
    return:
        age (int) : number of years
    """

    if birth_date is None or birth_date in [""]:
        return 0, "Pass a valid Date"
    birth_date = birth_date.replace('/', '-').replace(' ', '').replace(',', '')
    if birth_date is None or birth_date in [""]:
        return 0, "Pass a valid Date"
    try:
        birth_date = datetime.strptime(birth_date, '%d-%m-%Y')
        today = date.today()
        age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
        return int(age), None
    except:
        try:
            birth_date = datetime.strptime(birth_date, '%Y-%m-%d')
            today = date.today()
            age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
            return int(age), None
        except Exception as exception:
            return 0, exception


def get_address_details(qde_data):
    app_residence_stay_duration_yr = 0
    app_residence_stay_duration_mnth = 0
    app_residence_ownership_status = None
    app_residence_pincode = None
    app_residence_city = None
    app_residence_locality = None
    app_office_stay_duration_yr = 0
    app_office_ownership_status = None
    app_office_pincode = None
    app_office_city = None
    app_office_locality = None

    for address in qde_data['addresses']:
        address_type = address['type'].upper()
        if address_type in ['RESIDENCE', 'CURRENT']:
            app_residence_city = address.get('city', None)
            app_residence_pincode = int(address.get('pincode', 0))
            app_residence_ownership_status = str(address.get("ownershipStatus", None)).upper()
            try:
                app_residence_stay_duration_yr = int(address.get("stayDurationInMonths", 0)) / 12
                app_residence_stay_duration_mnth = int(address.get("stayDurationInMonths", 0))
            except:
                app_residence_stay_duration_yr = 0
                app_residence_stay_duration_mnth = 0
            try:
                app_residence_locality = str(address.get('locality', None)).upper()
            except:
                app_residence_locality = None

        if address_type in ['OFFICE']:
            app_office_city = str(address.get('city', None)).upper()
            app_office_pincode = int(address.get('pincode', None))
            app_office_ownership_status = str(address.get("ownershipStatus", None)).upper()
            try:
                app_office_stay_duration_yr = int(address.get("stayDurationInMonths", 0)) / 12
            except:
                app_office_stay_duration_yr = 0
            try:
                app_office_locality = str(address.get('locality', None)).upper()
            except:
                app_office_locality = None

    return app_residence_ownership_status, app_residence_stay_duration_mnth, app_residence_pincode, \
        app_office_ownership_status, app_office_pincode, app_office_city, app_office_stay_duration_yr


def occupation_details(applicant_qde_data):
    app_post_qualification_experience_in_months = 0
    app_other_entity_business_vintage = 0
    app_business_vintage = 0
    qde_partnership_firm = None
    if len(applicant_qde_data["occupationDetails"]) > 1:
        for occupation_data in applicant_qde_data["occupationDetails"]:
            if str(occupation_data["companyName"]).replace(" ", "").lower() not in ["", "none", "null"]:
                try:
                    app_other_entity_business_vintage = int(occupation_data["businessVintage"])
                except:
                    app_other_entity_business_vintage = 0
            else:
                try:
                    app_business_vintage = int(occupation_data["businessVintage"])
                except:
                    app_business_vintage = 0

            if str(occupation_data["companyName"]).replace(" ", "").lower() in ["", "none", "null"]:
                try:
                    app_post_qualification_experience_in_months = int(occupation_data["postQualificationExpInMonths"])
                except:
                    app_post_qualification_experience_in_months = 0
                try:
                    qde_partnership_firm = occupation_data.get('corporateStructure')
                except:
                    qde_partnership_firm = ""

    else:
        try:
            app_business_vintage = int(applicant_qde_data["occupationDetails"][0]["businessVintage"])
        except:
            app_business_vintage = 0
        try:
            app_post_qualification_experience_in_months = int(applicant_qde_data["occupationDetails"][0]["postQualificationExpInMonths"])
        except:
            app_post_qualification_experience_in_months = 0
        try:
            qde_partnership_firm = applicant_qde_data["occupationDetails"][0]['corporateStructure']
        except:
            qde_partnership_firm = ""

    return app_business_vintage, app_other_entity_business_vintage, \
        app_post_qualification_experience_in_months, qde_partnership_firm


def common_qde_policy_applicants(applicant_qde_data, applicant_age, app_residence_ownership_status, app_residence_stay_duration_mnth,
                                 app_office_ownership_status, qualification_type,
                                 app_office_stay_duration_yr, post_qualification_exp):
    if 25 <= applicant_age <= 65:
        pol_age_match = "PASS"
    else:
        pol_age_match = "FAIL"

    if app_residence_ownership_status in ["OWNED"] or (
            app_residence_ownership_status in ["RENTED"] and app_residence_stay_duration_mnth >= 6):
        pol_app_residence_match = "PASS"
    else:
        pol_app_residence_match = "RTC"

    if app_office_ownership_status in ["OWNED"]:
        pol_app_office_match = "PASS"
    elif (app_office_ownership_status in ["RENTED"] and qualification_type in ["Doctor Case"] and
          app_office_stay_duration_yr >= 1):
        pol_app_office_match = "PASS"
    elif (app_office_ownership_status in ["RENTED"] and qualification_type in ["Non Doctor Case"] and
          app_office_stay_duration_yr >= 2):
        pol_app_office_match = "PASS"
    elif qualification_type in ["Reject Doctor Case"]:
        pol_app_office_match = "FAIL"
    else:
        pol_app_office_match = "RTC"

    if qualification_type in ["Doctor Case"]:
        pol_post_qualification_exp = "PASS" if post_qualification_exp >= 36 else "FAIL"
    else:
        pol_post_qualification_exp = "PASS"

    if bool(applicant_qde_data["anyBuildUpProperty"]):
        pol_any_property_own = "PASS"
    else:
        pol_any_property_own = "Fail"

    return pol_age_match, pol_app_residence_match, pol_app_office_match, \
        pol_post_qualification_exp, pol_any_property_own


def hfs_qde_data(applicants, loan_details, collaterals, negative_area_pincodes, city_category_mapping,
                 qualification_type, qualification_list):
    tenure = loan_details.get('tenure', 0)
    applicant_data = None
    promoter_data = None
    corporate_structure_list = ["Individual", "Proprietorship", "Partnership", "Private Limited",
                                "Limited Company", "LLP", "Trust", "Society", "Others"]

    doctor_list = ["mbbs", "specialist(md/ms)", "mbbs+diploma"]
    non_doctor_list = ["homeopathy/unani/ayurvedic"]
    rejected_doctor_list = ["dentist(bds/mds)", "russiandoctor", "chinesedoctor"]

    app_user_type = None
    applicant_qde_data = {}
    promoter_qde_data = {}
    app_qualification = None
    app_business_vintage = 0
    app_other_entity_business_vintage = 0
    app_post_qualification_experience_in_months = 0
    app_partnership_firm = None
    applicant_age = 0
    promoter_age = 0
    co_applicant_age_list = []
    promoter_qualification = None
    app_residence_pincode = None
    app_office_pincode = None
    app_office_city = None
    app_residence_ownership_status = None
    app_office_ownership_status = None
    promoter_office_ownership_status = None
    promoter_residence_ownership_status = None
    app_office_stay_duration_yr = 0
    app_residence_stay_duration_mnth = 0
    co_applicant_office_ownership_status_list = []
    co_app_qualification_list = []
    co_applicant_residence_ownership_status_list = []
    multi_pol_co_applicant_age_match_list = []
    multi_pol_co_applicant_residence_match_list = []
    multi_pol_co_applicant_office_match_list = []
    multi_pol_co_applicant_post_qualification_exp_list = []
    multi_pol_co_applicant_any_property_own_list = []

    pol_app_age_match = None
    pol_promoter_age_match = None
    is_user_promoter = False
    pol_app_residence_match = None
    pol_promoter_residence_match = None
    pol_app_office_match = None
    pol_promoter_office_match = None
    pol_app_business_vintage = None
    pol_promoter_business_vintage = None
    pol_app_post_qualification_exp = None
    pol_promoter_post_qualification_exp = None
    pol_app_any_property_own = None
    pol_promoter_any_property_own = None
    pol_refurbished_asset_type = None

    for applicant in applicants:
        if str(applicant['userRole']).upper() == 'APPLICANT':
            applicant_data = applicant
            applicant_qde_data = applicant_data.get('qde')
            app_user_type = str(applicant['userType']).upper()
            if app_user_type == 'INDIVIDUAL':
                applicant_dob = applicant_qde_data.get('dob')
                applicant_age, error = age_calculator(applicant_dob)
            else:
                applicant_age = 0

            app_business_vintage, app_other_entity_business_vintage, \
                app_post_qualification_experience_in_months, app_partnership_firm = occupation_details(applicant_qde_data)

            app_residence_ownership_status, app_residence_stay_duration_mnth, app_residence_pincode, \
                app_office_ownership_status, app_office_pincode, app_office_city, \
                app_office_stay_duration_yr = get_address_details(applicant_qde_data)

            pol_app_age_match, pol_app_residence_match, pol_app_office_match, \
                pol_app_post_qualification_exp, pol_app_any_property_own = common_qde_policy_applicants(applicant_qde_data,applicant_age,
                                                                                      app_residence_ownership_status,
                                                                                      app_residence_stay_duration_mnth,
                                                                                      app_office_ownership_status,
                                                                                      qualification_type,
                                                                                      app_office_stay_duration_yr,
                                                                                      app_post_qualification_experience_in_months)

        elif str(applicant['userRole']).upper() == 'PROMOTER':
            promoter_data = applicant
            promoter_qde_data = promoter_data.get('qde')
            is_user_promoter = True
            if str(applicant['userType']).upper() == 'INDIVIDUAL':
                promoter_dob = promoter_qde_data.get('dob')
                promoter_age, error = age_calculator(promoter_dob)
            else:
                promoter_age = 0

            promoter_business_vintage, promoter_other_entity_business_vintage, \
                promoter_post_qualification_experience_in_months, promoter_partnership_firm = occupation_details(promoter_qde_data)

            promoter_residence_ownership_status, promoter_residence_stay_duration_mnth, promoter_residence_pincode, \
                promoter_office_ownership_status, promoter_office_pincode, \
                promoter_office_city, promoter_office_stay_duration_yr = get_address_details(promoter_qde_data)

            pol_promoter_age_match, pol_promoter_residence_match, \
                pol_promoter_office_match, pol_promoter_post_qualification_exp, \
                pol_promoter_any_property_own = common_qde_policy_applicants(promoter_qde_data, promoter_age,
                                                                         promoter_residence_ownership_status,
                                                                         promoter_residence_stay_duration_mnth,
                                                                         promoter_office_ownership_status,
                                                                         qualification_type,
                                                                         promoter_office_stay_duration_yr,
                                                                         promoter_post_qualification_experience_in_months)


        else:
            co_applicant_data = applicant
            co_applicant_qde_data = co_applicant_data.get('qde')

            if str(applicant['userType']).upper() == 'INDIVIDUAL':
                co_applicant_dob = co_applicant_qde_data.get('dob')
                co_applicant_age, error = age_calculator(co_applicant_dob)
            else:
                co_applicant_age = 0

            co_applicant_age_list.append(co_applicant_age)

            co_app_business_vintage, co_app_other_entity_business_vintage, \
                co_app_post_qualification_experience_in_months, co_app_partnership_firm = occupation_details(co_applicant_qde_data)

            co_applicant_residence_ownership_status, co_app_residence_stay_duration_mnth, co_app_residence_pincode, \
                co_applicant_office_ownership_status, co_applicant_office_pincode, co_applicant_office_city, \
                co_applicant_office_stay_duration_yr = get_address_details(co_applicant_qde_data)

            co_applicant_office_ownership_status_list.append(co_applicant_office_ownership_status)
            co_applicant_residence_ownership_status_list.append(co_applicant_residence_ownership_status)

            pol_co_applicant_age_match, pol_co_applicant_residence_match, \
                pol_co_applicant_office_match, pol_co_applicant_post_qualification_exp, \
                pol_co_applicant_any_property_own = common_qde_policy_applicants(co_applicant_qde_data, co_applicant_age,
                                                                             co_applicant_residence_ownership_status,
                                                                             co_app_residence_stay_duration_mnth,
                                                                             co_applicant_office_ownership_status,
                                                                             qualification_type,
                                                                             co_applicant_office_stay_duration_yr,
                                                                             co_app_post_qualification_experience_in_months)

            multi_pol_co_applicant_age_match_list.append(pol_co_applicant_age_match)
            multi_pol_co_applicant_residence_match_list.append(pol_co_applicant_residence_match)
            multi_pol_co_applicant_office_match_list.append(pol_co_applicant_office_match)
            multi_pol_co_applicant_post_qualification_exp_list.append(pol_co_applicant_post_qualification_exp)
            multi_pol_co_applicant_any_property_own_list.append(pol_co_applicant_any_property_own)

    # ================================================================================================
    # ================================================================================================

    if qualification_type == 'Doctor Case':
        pol_app_business_vintage = "PASS" if app_business_vintage > 12 else "RTC"
    elif qualification_type == 'Non Doctor Case':
        if applicant_qde_data.get("additionalVintage", False):
            pol_app_business_vintage = "PASS" if app_other_entity_business_vintage > 36 else "FAIL"
        else:
            pol_app_business_vintage = "PASS" if app_business_vintage > 36 else "FAIL"
    else:
        pol_app_business_vintage = "FAIL"

    multi_pol_co_applicant_age_match_list.append(pol_app_age_match)
    multi_pol_co_applicant_age_match_list.append(pol_promoter_age_match)
    pol_age_match = "PASS" if any(i == "PASS" for i in multi_pol_co_applicant_age_match_list) else "FAIL"

    multi_pol_co_applicant_residence_match_list.append(pol_app_residence_match)
    multi_pol_co_applicant_residence_match_list.append(pol_promoter_residence_match)
    pol_residence_match = "PASS" if any(i == "PASS" for i in multi_pol_co_applicant_residence_match_list) else "RTC"

    multi_pol_co_applicant_office_match_list.append(pol_app_office_match)
    multi_pol_co_applicant_office_match_list.append(pol_promoter_office_match)
    if any(i == "PASS" for i in multi_pol_co_applicant_office_match_list):
        pol_office_match = "PASS"
    elif all(i == "FAIL" for i in multi_pol_co_applicant_office_match_list):
        pol_office_match = "FAIL"
    else:
        pol_office_match = "RTC"

    multi_pol_co_applicant_post_qualification_exp_list.append(pol_app_post_qualification_exp)
    multi_pol_co_applicant_post_qualification_exp_list.append(pol_promoter_post_qualification_exp if is_user_promoter else "FAIL")
    pol_post_qualification_exp = "PASS" if any(i == "PASS" for i in multi_pol_co_applicant_post_qualification_exp_list) else "FAIL"

    multi_pol_co_applicant_any_property_own_list.append(pol_app_any_property_own)
    multi_pol_co_applicant_any_property_own_list.append(pol_promoter_any_property_own)
    pol_any_property_own = "PASS" if any(i == "PASS" for i in multi_pol_co_applicant_any_property_own_list) else "FAIL"

    if applicant_age not in [0]:
        age_1 = applicant_age
    elif promoter_age not in [0]:
        age_1 = promoter_age
    else:
        try:
            age_1 = min(co_applicant_age_list)
        except:
            age_1 = 0

    try:
        age_2 = min(co_applicant_age_list) if promoter_age == 0 else promoter_age
    except:
        age_2 = 0

    if app_office_pincode not in ["", None, "NONE", "NULL"]:
        try:
            app_office_location_category = \
            city_category_mapping[city_category_mapping['Pincode'] == str(app_office_pincode)]['Location Category'].values[0]
        except:
            try:
                app_office_location_category = \
                city_category_mapping[city_category_mapping['City'].str.upper() == str(app_office_city).upper()]['Location Category'].values[0]
            except:
                app_office_location_category = None
    else:
        app_office_location_category = None

    mumbai_branch = 1   # if app_office_city in ['MUMBAI'] else 0

    bangalore_branch = 1    # if app_office_city in ['BANGLORE', 'BANGALORE'] else 0

    delhi_branch = 1    # if app_office_city in ['DELHI', 'NEW DELHI'] else 0

    co_applicant_office_ownership_status_list.append(app_office_ownership_status)
    co_applicant_office_ownership_status_list.append(promoter_office_ownership_status)

    office_owned = 1 if any("OWNED" == status for status in co_applicant_office_ownership_status_list) else 0

    co_applicant_residence_ownership_status_list.append(app_residence_ownership_status)
    co_applicant_residence_ownership_status_list.append(promoter_residence_ownership_status)

    residence_owned = 1 if any("OWNED" == status for status in co_applicant_residence_ownership_status_list) else 0

    constitution_relationship2_partner = 0
    constitution_relationship2_proprietor = 0

    if app_partnership_firm in ['Partnership']:
        constitution_relationship2_partner = 1
    elif app_partnership_firm in ['Proprietorship']:
        constitution_relationship2_proprietor = 1

    if app_user_type == 'INDIVIDUAL':
        if app_residence_pincode is not None and app_residence_pincode in negative_area_pincodes['pincode'].values:
            pol_app_negative_area_match = "FAIL"
        else:
            pol_app_negative_area_match = "PASS"
    else:
        if app_office_pincode is not None and app_office_pincode in negative_area_pincodes['pincode'].values:
            pol_app_negative_area_match = "FAIL"
        else:
            pol_app_negative_area_match = "PASS"

    if qualification_type in ["Doctor Case", "Non Doctor Case"]:
        pol_qualification = "PASS"
    else:
        pol_qualification = "FAIL"
    # ==============================================================================

    # need shareHolding percentage parameter in qde
    if app_partnership_firm in ["Partnership"]:
        share_holding = applicant_qde_data.get("shareHolding", 0.5)
        share_holding_2 = promoter_qde_data.get("shareHolding", 0.5)
    else:
        share_holding = applicant_qde_data.get("shareHolding", 0.5)
        share_holding_2 = promoter_qde_data.get("shareHolding", 0.5)

    # ==============================================================================
    # ------------------------------Demographic segment block-----------------------
    # ==============================================================================

    asset_tpe_cat_a_list = ["MRI", "CT", "CT Scanner", "Mammography", "Ultrasound", "BMD", "X-Ray"]
    asset_tpe_cat_b_list = ["Ventilator", "Anesthesia", "Endoscopy", "CR System", "DR System", "Pathology", "Microscopes",
                            "Laparoscopy"]
    asset_tpe_cat_c_list = ["Ophthalmology", "Defibrillator", "Dialysis Machines", "Others"]

    try:
        total_asset_value = sum([float(row["assetPrice"]) * float(row["assetUnits"]) for row in collaterals])
    except:
        total_asset_value = 0

    try:
        for row in collaterals:
            row["assetValue"] = float(row["assetPrice"]) * float(row["assetUnits"])
            row["percentage"] = total_asset_value and (row["assetValue"] / total_asset_value) * 100 or 0

        perc_max = max([row["percentage"] for row in collaterals])

        max_perc_collateral = [row for row in collaterals if row["percentage"] == perc_max]
        try:
            asset_type = str(max_perc_collateral[0]["typeOfAsset"]).upper()
            asset_condition = str(max_perc_collateral[0]["assetCondition"]).lower()
        except:
            asset_type = None
            asset_condition = None
    except Exception as e:
        error = e
        asset_type = None
        asset_condition = None
        asset_value = 0
        max_perc_collateral = []

    if asset_condition in ["refurbished"]:
        pol_refurbished_asset_type = "PASS" if asset_type in ["CT", "CT Scanner", "MRI"] else "FAIL"
    else:
        pol_refurbished_asset_type = "PASS"

    if int(total_asset_value) in [0]:
        pol_total_asset_value = "FAIL"
    else:
        pol_total_asset_value = "PASS"

    try:
        asset_type_cat = str(max_perc_collateral[0]["assetCategory"]).lower()
        if asset_type_cat in ["other", "others", "none"]:
            asset_type_cat = 'cat c'
    except:
        if asset_type in asset_tpe_cat_a_list:
            asset_type_cat = "cat a"
        elif asset_type in asset_tpe_cat_b_list:
            asset_type_cat = "cat b"
        elif asset_type in asset_tpe_cat_c_list:
            asset_type_cat = "cat c"
        else:
            asset_type_cat = 'cat c'

    if asset_condition in ["refurbished"]:
        asset_type_cat = "cat b" if asset_type in ["CT", "CT Scanner", "MRI"] else asset_type_cat

    if asset_type_cat in ["a"]:
        asset_type_cat = 'cat a'
    elif asset_type_cat in ["b"]:
        asset_type_cat = "cat b"
    elif asset_type_cat in ["c"]:
        asset_type_cat = "cat c"

    if app_business_vintage >= 84:
        business_vintage_score = 12
    elif 48 <= app_business_vintage < 84:
        business_vintage_score = 6
    elif app_business_vintage < 48:
        business_vintage_score = 3
    else:
        business_vintage_score = 0

    qualification_score_list = [0]
    for qualification in qualification_list:
        if qualification in ["specialist(md/ms)", "mbbs+diploma"]:
            qualification_score_list.append(12)
        elif qualification in ["mbbs"]:
            qualification_score_list.append(6)
        elif qualification in ["homeopathy/unani/ayurvedic", "others"]:
            qualification_score_list.append(3)
        else:
            qualification_score_list.append(0)

    qualification_score = max(qualification_score_list)

    if any("OWNED" == status for status in co_applicant_office_ownership_status_list):
        app_office_ownership_status_score = 12
    else:
        app_office_ownership_status_score = 6

    if any("OWNED" == status for status in co_applicant_residence_ownership_status_list):
        app_residence_ownership_status_score = 12
    else:
        app_residence_ownership_status_score = 6

    if app_office_location_category in ["a", "A"]:
        app_office_city_score = 12
    elif app_office_location_category in ["b", "B"]:
        app_office_city_score = 6
    elif app_office_location_category in ["c", "C"]:
        app_office_city_score = 3
    else:
        app_office_city_score = 0

    total_score = business_vintage_score + qualification_score + app_office_ownership_status_score + \
                  app_residence_ownership_status_score + app_office_city_score

    if total_score <= 22:
        score_band = "D5"
    elif 22 < total_score <= 29:
        score_band = "D4"
    elif 29 < total_score <= 36:
        score_band = "D3"
    elif 36 < total_score <= 43:
        score_band = "D2"
    elif total_score > 43:
        score_band = "D1"
    else:
        score_band = "D5"

    # ==============================================================================
    # All rules running on any applicants logic except "pol_refurbished_asset_type"
    '''
    # pol_residence_match --> PASS, RTC
    # pol_office_match --> PASS, RTC
    # pol_post_qualification_exp --> PASS,FAIL
    # pol_business_vintage --> PASS, RTC, FAIL
    # pol_age_match --> PASS, FAIL
    # pol_any_property_own --> PASS, FAIL for S3 or S4 Band
    # pol_refurbished_asset_type --> PASS, FAIL
    '''

    if pol_age_match == "PASS" and \
            pol_qualification == "PASS" and \
            pol_refurbished_asset_type == "PASS" and \
            pol_app_negative_area_match == "PASS" and \
            pol_office_match in ["PASS", "RTC"] and \
            pol_app_business_vintage in ["PASS", "RTC"] and \
            pol_post_qualification_exp == "PASS" and \
            pol_total_asset_value == "PASS":

        if pol_residence_match == "RTC" or \
                pol_office_match == "RTC" or \
                pol_app_business_vintage == "RTC":
            pol_decision_status = "Refer"
        elif pol_app_business_vintage == "FAIL":
            pol_decision_status = "Decline"
        else:
            pol_decision_status = "Accept"
    else:
        pol_decision_status = "Decline"

    if pol_decision_status in ["Decline", "Refer"]:
        rejection_reason = qde_rejection_reasons(pol_post_qualification_exp, pol_qualification, pol_app_business_vintage, pol_age_match,
                                                 pol_residence_match, pol_office_match,
                                                 pol_refurbished_asset_type, pol_app_negative_area_match, pol_total_asset_value)
    else:
        rejection_reason = []

    # ==============================================================================
    dict1 = {
        'Office_Owned': office_owned,
        'Office_Stability': app_office_stay_duration_yr,
        'Resi_Stability': int(app_residence_stay_duration_mnth/12),
        'Resi_Owned': residence_owned,
        'AGE2': age_2,
        'AGE1': age_1,
        'Industry_Margin': 0.07,
        'Tenor': tenure,
        'Shareholding2': share_holding_2,
        'Shareholding1': share_holding,
        'Business_Type_manufactur': 0,
        'Business_Type_trade': 0,
        'Branch_banglore': bangalore_branch,
        'Branch_delhi': delhi_branch,
        'Branch_mumbai': mumbai_branch,
        'Constitution/Relationship2_partner': constitution_relationship2_partner,
        'Constitution/Relationship2_proprietor': constitution_relationship2_proprietor,
        "pol_app_negative_area_match": pol_app_negative_area_match,
        "pol_decision_status": pol_decision_status,
        "rejection_reason": rejection_reason,
        "qualification_type": qualification_type,
        "app_partnership_firm": app_partnership_firm,
        "app_other_entity_business_vintage": app_other_entity_business_vintage,
        "app_business_vintage": app_business_vintage,
        "pol_age_match": pol_age_match,
        "pol_any_property_own": pol_any_property_own,
        "pol_qualification": pol_qualification,
        "pol_refurbished_asset_type": pol_refurbished_asset_type,
        "pol_post_qualification_exp": pol_post_qualification_exp,
        "pol_residence_match": pol_residence_match,
        "pol_office_match": pol_office_match,
        "pol_business_vintage": pol_app_business_vintage,
        "total_asset_value": total_asset_value,
        "total_score": total_score,
        "business_vintage_score": business_vintage_score,
        "qualification_score": qualification_score,
        "app_office_ownership_status_score": app_office_ownership_status_score,
        "app_residence_ownership_status_score": app_residence_ownership_status_score,
        "app_office_city_score": app_office_city_score
    }

    return dict1, score_band, asset_type_cat, total_asset_value, max_perc_collateral, \
        asset_condition, asset_type
