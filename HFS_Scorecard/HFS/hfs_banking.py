from functools import reduce
import math
import dateutil.relativedelta
import re
from fuzzywuzzy import fuzz
import numpy as np
import os
import sys
from dateutil import parser
from datetime import datetime, date
from HFS.hfs_qde import *
from HFS.rejection_reasons import *


def name_cleaner(name):
    new_name = ''
    if name is not None:
        new_name = re.sub(r'^(mr|mister|mrs|ms|miss|dr|prof)[\.\s]*', r'', name, count=1,
                          flags=re.IGNORECASE).strip().lower()
    return new_name


def name_match_with_perfios(bank_details, qde_full_name, applicant_type):

    false_name_match_banking = []
    pol_name_match_list = []
    for row in bank_details:
        perfios_customer_name = str(deep_get(row, 'analysisData.customerInfo.name')).lower()

        pf_name_check_fuzz_ratio = 0
        if qde_full_name is not None and perfios_customer_name is not None:
            pf_name_check_fuzz_ratio = fuzz.ratio(name_cleaner(qde_full_name), name_cleaner(perfios_customer_name))

        if pf_name_check_fuzz_ratio >= 80 or all(str(name).lower() in perfios_customer_name for name in qde_full_name.split(" ")):
            pol_name_match_list.append(True)
        else:
            pol_name_match_list.append(False)
            false_name_match_banking.append(row)

    if applicant_type in ["APPLICANT"]:
        pol_name_match = False if len(pol_name_match_list) == 0 else all(pol_name_match_list)
    else:
        pol_name_match = any(pol_name_match_list)

    return pol_name_match, false_name_match_banking


def abb_func(row, is_applicant_individual, is_prop_corp_structure, qde_pan_no):
    if (row["analysisData"]["summaryInfo"]["accType"] in ["Current Account", "Current", "CA - BUSINESS CLASSIC", "CA-REGULAR-PUB-OTH-ALL-INR"]) or \
            ((is_applicant_individual or is_prop_corp_structure) and row["analysisData"]["summaryInfo"]["accType"] in ["Saving Account", "Saving", "REGULAR SB CHQ-INDIVIDUALS URAL-INR", "REGULAR SB CHQ-INDIVIDUALS"]) or \
            qde_pan_no in ["BVCPP3620H"]:
        avg_bal = row['analysisData']['summaryInfo']['average']['balAvg']
        # full_month_count = row['analysisData']['summaryInfo']['fullMonthCount']
        try:
            abb = float(avg_bal)*100000
        except Exception as error:
            abb = 0
    else:
        abb = None
    return abb


def abb_func_debit_credit_based(row, is_applicant_individual, is_prop_corp_structure, qde_pan_no):
    acc_type = row["analysisData"]["summaryInfo"]["accType"]
    avg_bal_list = []
    if ("ca" in acc_type or "current" in acc_type) or ((is_applicant_individual or is_prop_corp_structure) and ("sb" in acc_type or "saving" in acc_type)):
        for monthly_details in row['analysisData']['monthlyDetails']:
            if monthly_details.get("debits", 0) > 0 or monthly_details.get("creditsBT", 0) > 0:
                try:
                    avg_bal_list.append(float(monthly_details.get("balAvg", 0))*100000)
                except Exception as error:
                    avg_bal_list.append(0)

    try:
        abb = sum(avg_bal_list)/len(avg_bal_list)
    except:
        abb = None
    return abb


def deep_get(dictionary, keys, default=0):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)


def month_count_fun(start_date):
    try:
        today = date.today()
        start_date = datetime.strptime(start_date, '%Y-%m-%d').date()

        day = int(((today - start_date).days / 30))

        return day
    except Exception as error:
        return


def date_in_range_count(month_yy_string, months):
    try:
        today = date.today()
        try:
            start_date = datetime.strptime(month_yy_string, '%b-%y').date()
        except:
            start_date = datetime.strptime(month_yy_string, '%Y-%m-%d').date()

        req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
        if req_date <= start_date <= today:
            return True
        else:
            return False
    except Exception as error:
        return False


def get_months_number(row):
    last_2_months_list = []
    for month_details in row["analysisData"]["monthlyDetails"]:
        try:
            num_month = datetime.strptime(str(deep_get(month_details, 'monthName')), '%b-%y').date().month
        except TypeError as e:
            num_month = 0
        last_2_months_list.append(num_month)

    last_month = max(last_2_months_list)
    last_2_months_list.remove(last_month)
    second_last_month = max(last_2_months_list)
    return [last_month, second_last_month]


def emi_bounce_count(emi_bounce, month_yy_string, last_2_months_list, months):
    try:
        # today = date.today()
        try:
            num_month = datetime.strptime(month_yy_string, '%b-%y').date().month
        except:
            num_month = datetime.strptime(month_yy_string, '%Y-%m-%d').date().month

        # req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
        if num_month in last_2_months_list:
            if emi_bounce in [None, ''] or float(emi_bounce) == 0:
                return True
            else:
                return False
        else:
            return False
    except Exception as error:
        return False


def get_inw_out_bounce_x_mth_gt_lte_debits(bank_details, mnth, debits_no, operator, bounce_type):
    inward_bounce_list_x_months = []

    bonce_type = f"analysisData.summaryInfo.total.{bounce_type}"

    for row in bank_details:
        if deep_get(row, 'analysisData.summaryInfo.fullMonthCount') <= mnth:
            if operator == "GT":
                if deep_get(row, 'analysisData.summaryInfo.total.debits') > debits_no:
                    inward_bounce_list_x_months.append(deep_get(row, bonce_type))
                else:
                    inward_bounce_list_x_months.append(0)
            else:
                if deep_get(row, 'analysisData.summaryInfo.total.debits') <= debits_no:
                    inward_bounce_list_x_months.append(deep_get(row, bonce_type))
                else:
                    inward_bounce_list_x_months.append(0)
        else:
            months_debit_count_list = []
            month_inw_bounces_list = []
            for month in row["analysisData"]["summaryInfo"]["monthlyDetails"]:
                if month_count_fun(month["startDate"]) <= mnth:
                    months_debit_count_list.append(deep_get(month, 'debits'))
                    month_inw_bounces_list.append(deep_get(month, str(bounce_type)))
            if operator == "GT":
                if sum(months_debit_count_list) > debits_no:
                    inward_bounce_list_x_months.append(sum(month_inw_bounces_list))
                else:
                    inward_bounce_list_x_months.append(0)
            else:
                if sum(months_debit_count_list) <= debits_no:
                    inward_bounce_list_x_months.append(sum(month_inw_bounces_list))
                else:
                    inward_bounce_list_x_months.append(0)

    return sum(inward_bounce_list_x_months)


def get_banking(bank_details, applicants, applicant_data, pol_doctor_startup):
    co_app_name_match_list = []
    pol_name_match = False
    org_bank_details = bank_details
    business_vintage, other_business_vintage, post_qualification_experience, \
        qde_partnership_firm = occupation_details(applicant_data.get('qde', {}))

    is_applicant_individual = True if applicant_data.get("userType") == "INDIVIDUAL" else False
    is_prop_corp_structure = True if qde_partnership_firm in ["Proprietorship", "Proprietorship Firm"] else False

    qde_pan_no = f"{deep_get(applicant_data, 'qde.pan')}"

    qde_full_name = f"{deep_get(applicant_data, 'qde.firstName')} {deep_get(applicant_data, 'qde.lastName')}"
    pol_name_match, false_name_match_banking = name_match_with_perfios(bank_details, qde_full_name, 'APPLICANT')

    if qde_partnership_firm in ["Proprietorship", "Proprietorship Firm"] and pol_name_match is False:
        co_app_name_match_list = []
        for applicant in applicants:
            if not str(applicant['userRole']).upper() == 'APPLICANT':
                business_vintage, other_business_vintage, post_qualification_experience, \
                    qde_co_app_partnership_firm = occupation_details(applicant.get('qde', {}))
                if qde_co_app_partnership_firm in ["Proprietorship", "Proprietorship Firm"]:
                    qde_full_name = f"{deep_get(applicant, 'qde.firstName')} {deep_get(applicant, 'qde.lastName')}"
                    pol_name_match, false_name_match_banking_co_app = name_match_with_perfios(false_name_match_banking, qde_full_name, 'CO-APPLICANT')
                    co_app_name_match_list.append(pol_name_match)

        if len(co_app_name_match_list) != 0 and all(co_app_name_match_list):
            pol_name_match = True
        else:
            pol_name_match = False
    else:
        pol_name_match = pol_name_match

    no_of_banks_after_name_match = len(bank_details)
    no_of_banks = len(org_bank_details)
    if no_of_banks > 0:
        # abb_list = [abb_value for abb_value in [abb_func(row, is_applicant_individual, is_prop_corp_structure, qde_pan_no) for row in bank_details] if abb_value is not None]

        abb_list = [abb_value for abb_value in [abb_func_debit_credit_based(row, is_applicant_individual, is_prop_corp_structure, qde_pan_no) for row in bank_details] if abb_value is not None]

        full_month_count_list = [deep_get(row, 'analysisData.summaryInfo.fullMonthCount') for row in bank_details]
        pol_bank_details_gt_6_mnth = any(x >= 6 for x in full_month_count_list)
        total_abb = sum(abb_list)
        avg_abb = total_abb
        max_abb = max(abb_list) if len(abb_list) > 0 else 0
        min_abb = min(abb_list) if len(abb_list) > 0 else 0
        total_credits = sum([deep_get(row, 'analysisData.summaryInfo.total.creditsBT') for row in bank_details])
        total_credits_current = sum([deep_get(row, 'analysisData.summaryInfo.total.creditsBT') for row in bank_details if row["analysisData"]["summaryInfo"]["accType"] in ["Current Account", "Current", "NONE"]])
        total_debits_current = sum([deep_get(row, 'analysisData.summaryInfo.total.debits') for row in bank_details if row["analysisData"]["summaryInfo"]["accType"] in ["Current Account", "Current", "NONE"]])
        total_credits_ccod = sum([deep_get(row, 'analysisData.summaryInfo.total.creditsBT') for row in bank_details if row["analysisData"]["summaryInfo"]["accType"] in ["CCOD", "CC", "OD", "Overdraft", "Cash Credit"]])
        total_debits_ccod = sum([deep_get(row, 'analysisData.summaryInfo.total.debits') for row in bank_details if row["analysisData"]["summaryInfo"]["accType"] in ["CCOD", "CC", "OD", "Overdraft", "Cash Credit"]])
        total_debits_ccod_amt = sum([deep_get(row, 'analysisData.summaryInfo.total.totalDebit') for row in bank_details if row["analysisData"]["summaryInfo"]["accType"] in ["CCOD", "CC", "OD", "Overdraft", "Cash Credit"]])

        inward_bounce_list = [deep_get(row, 'analysisData.summaryInfo.total.inwBounces') for row in bank_details]
        outward_bounce_list = [deep_get(row, 'analysisData.summaryInfo.total.outwBounces') for row in bank_details]

        total_inward_bounce = sum(inward_bounce_list)
        max_inward_bounce = max(inward_bounce_list) if len(inward_bounce_list) > 0 else 0

        total_outward_bounce = sum(outward_bounce_list)
        max_outward_bounce = max(outward_bounce_list) if len(outward_bounce_list) > 0 else 0

        total_debits = sum([deep_get(row, 'analysisData.summaryInfo.total.debits') for row in bank_details])

        try:
            perc_inward_bounce = total_inward_bounce / total_debits
        except ZeroDivisionError:
            perc_inward_bounce = 0

        pol_perc_inward_bounce = perc_inward_bounce <= 0.03

        try:
            perc_outward_bounce = total_outward_bounce / total_debits
        except ZeroDivisionError:
            perc_outward_bounce = 0

        pol_perc_outward_bounce = perc_outward_bounce <= 0.05

        # ==========================================================

        # ==========================================================

        total_credits_bt = sum([deep_get(row, 'analysisData.summaryInfo.total.creditsBT') for row in bank_details])
        is_six_month_details = all(len(deep_get(row, 'analysisData.monthlyDetails')) >= 6 for row in bank_details)

        credits_bt_list = []
        all_bank_date_range_count_list = []
        emi_bounce_list = []

        for row in bank_details:
            date_range_count_list = []
            is_not_emi_bounce_list = []
            last_2_months_list = get_months_number(row)
            for month_details in row["analysisData"]["monthlyDetails"]:
                credits_bt_list.append(deep_get(month_details, 'creditsBT') >= 5)
                is_not_emi_bounce = emi_bounce_count(deep_get(month_details, 'inwEMIBounces'), str(deep_get(month_details, 'monthName')), last_2_months_list, 2)
                is_not_emi_bounce_list.append(is_not_emi_bounce)
                # date_range_count_list.append(date_in_range_count(str(deep_get(month_details, 'monthName')), 6))

            emi_bounce_list.append(sum(is_not_emi_bounce_list) >= 2)
            all_bank_date_range_count_list.append(len(row["analysisData"]["monthlyDetails"]) >= 6)

        is_bank_last_6_mnth_details = any(all_bank_date_range_count_list)
        is_nil_emi_bounce_last_2_mnth = all(emi_bounce_list)

        pol_credits_bt = True if total_credits_bt >= 30 or all(credits_bt_list) else False

        # =========================================================================
        avg_util_each_bank_list = []
        for row in bank_details:
            avg_util_list = []
            if row["analysisData"]["summaryInfo"]["accType"] in ["CCOD", "CC", "OD", "Overdraft", "Cash Credit"]:
                for month_details in row["analysisData"]["AdditionalMonthlyDetails"]:
                    avg_util_list.append(deep_get(month_details, 'avgUtilization'))

                try:
                    avg_util_each_bank_list.append(sum(avg_util_list)/len(avg_util_list))
                except:
                    avg_util_each_bank_list.append(0)

        try:
            avg_cc_od_util = sum(avg_util_each_bank_list)/len(avg_util_each_bank_list)
        except:
            avg_cc_od_util = 0
        try:
            existing_annual_obligations_list = []
            for row in bank_details:
                obl_month_dict = {}
                avg_obl_each_bank_list = []
                for emi_xns in row["analysisData"]["eMIECSXns"]:
                    if int(datetime.strptime(emi_xns['date'], '%Y-%m-%d').month) in obl_month_dict:
                        try:
                            obl_month_dict[int(datetime.strptime(emi_xns['date'], '%Y-%m-%d').month)].append(abs(float(emi_xns["amount"])))
                        except:
                            obl_month_dict[int(datetime.strptime(emi_xns['date'], '%Y-%m-%d').month)].append(0)
                    else:
                        obl_month_dict[int(datetime.strptime(emi_xns['date'], '%Y-%m-%d').month)] = [abs(float(emi_xns["amount"]))]

                avg_obl_each_bank_list = [sum(v) for k, v in obl_month_dict.items()]
                try:
                    existing_annual_obligations_list.append((sum(avg_obl_each_bank_list) / len(avg_obl_each_bank_list)) * 12)
                except:
                    existing_annual_obligations_list.append(0)

            existing_annual_obligations = sum(existing_annual_obligations_list)
        except:
            existing_annual_obligations = 0

        pol_emi_bounces = sum([deep_get(row, 'analysisData.summaryInfo.total.inwEMIBounces') for row in bank_details]) <= 2 and is_nil_emi_bounce_last_2_mnth

        # =========================================================================
        is_doctor_startup_override = False

        if pol_perc_inward_bounce is True and \
                pol_perc_outward_bounce is True and \
                pol_credits_bt is True and \
                pol_emi_bounces is True and \
                pol_bank_details_gt_6_mnth is True and \
                is_bank_last_6_mnth_details is True:
            if pol_name_match is False:
                pol_decision_status = "Refer"
            else:
                pol_decision_status = "Accept"
        else:
            if pol_doctor_startup == "RTC":
                is_doctor_startup_override = True
                pol_decision_status = "Refer"
            else:
                pol_decision_status = "Decline"

        if pol_decision_status in ["Decline", "Refer"]:
            rejection_reason = bank_rejection_reasons(pol_perc_inward_bounce, pol_perc_outward_bounce,
                                                      pol_credits_bt, pol_bank_details_gt_6_mnth,
                                                      is_bank_last_6_mnth_details, pol_emi_bounces,
                                                      pol_name_match, pol_doctor_startup, is_doctor_startup_override)
        else:
            rejection_reason = []

        # ========================================================================
        dict1 = {'Average Bank Balance': float("{0:.4f}".format(avg_abb)),
                 'abb': float("{0:.4f}".format(avg_abb)),
                 'Credits': float("{0:.4f}".format(total_credits)),
                 'current_Nos. of Credit': float("{0:.4f}".format(total_credits_current)),
                 'current_Nos. of Debit': float("{0:.4f}".format(total_debits_current)),
                 'ccod_Nos. of Credit': float("{0:.4f}".format(total_credits_ccod)),
                 'ccod_Nos. of Debit': float("{0:.4f}".format(total_debits_ccod)),
                 'ccod_Debit Amount': float("{0:.4f}".format(total_debits_ccod_amt)),
                 'Average Bank Balance max': float("{0:.4f}".format(max_abb)),
                 'Average Bank Balance min': float("{0:.4f}".format(min_abb)),
                 'Inward_Bounce': float("{0:.4f}".format(perc_inward_bounce)),
                 'Avg_CC_OD_Util': float("{0:.4f}".format(avg_cc_od_util)),
                 'Existing_Annual_obligations': float("{0:.4f}".format(existing_annual_obligations)),
                 "no_of_banks": no_of_banks,
                 "pol_decision_status": pol_decision_status,
                 "rejection_reason": rejection_reason,
                 "max_abb": float("{0:.4f}".format(max_abb)),
                 "min_abb": float("{0:.4f}".format(min_abb)),
                 "perc_inward_bounce": float("{0:.4f}".format(perc_inward_bounce)),
                 "perc_outward_bounce": float("{0:.4f}".format(perc_outward_bounce)),
                 "total_inward_bounce": float("{0:.4f}".format(total_inward_bounce)),
                 "max_inward_bounce": float("{0:.4f}".format(max_inward_bounce)),
                 "max_outward_bounce": float("{0:.4f}".format(max_outward_bounce)),
                 "total_debits": float("{0:.4f}".format(total_debits)),
                 "total_credits_bt": total_credits_bt,
                 "total_outward_bounce": float("{0:.4f}".format(total_outward_bounce)),
                 "pol_perc_inward_bounce": pol_perc_inward_bounce,
                 "pol_perc_outward_bounce": pol_perc_outward_bounce,
                 "pol_bank_6_mnth_details": is_bank_last_6_mnth_details,
                 "pol_bank_details_gt_6_mnth": pol_bank_details_gt_6_mnth,
                 "pol_credits_bt": pol_credits_bt,
                 "pol_emi_bounces": pol_emi_bounces,
                 "pol_name_match": pol_name_match
                 }
        return dict1

    else:
        dict1 = {'Average Bank Balance': 0,
                 'abb': 0,
                 'Credits': 0,
                 'current_Nos. of Credit': 0,
                 'current_Nos. of Debit': 0,
                 'ccod_Nos. of Credit': 0,
                 'ccod_Nos. of Debit': 0,
                 'ccod_Debit Amount': 0,
                 'Average Bank Balance max': 0,
                 'Average Bank Balance min': 0,
                 'Inward_Bounce': 0,
                 'Avg_CC_OD_Util': 0,
                 'Existing_Annual_obligations': 0,
                 'no_of_banks_after_name_match': no_of_banks_after_name_match,
                 "no_of_banks": no_of_banks,
                 "pol_decision_status": "Decline",
                 "rejection_reason": [f"total no of bank details: {no_of_banks}"],
                 "max_abb": 0,
                 "min_abb": 0,
                 "perc_inward_bounce": 0,
                 "perc_outward_bounce": 0,
                 "total_inward_bounce": 0,
                 "max_inward_bounce": 0,
                 "max_outward_bounce": 0,
                 "total_debits": 0,
                 "total_credits_bt": 0,
                 "total_outward_bounce": 0,
                 "pol_perc_inward_bounce": False,
                 "pol_perc_outward_bounce": False,
                 "is_bank_last_6_mnth_details": False,
                 "pol_bank_details_gt_6_mnth": False,
                 "pol_credits_bt": False,
                 "pol_emi_bounces": False,
                 "pol_name_match": False,
                 "pol_num_banks": False
                 }
        return dict1

