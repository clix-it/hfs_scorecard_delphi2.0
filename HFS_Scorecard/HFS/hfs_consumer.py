import logging
import numpy as np
import dateutil.relativedelta
import pandas as pd
from datetime import datetime
from functions.conf_functions import *
from common_functions import *
from HFS.rejection_reasons import *
from fuzzywuzzy import fuzz


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def hfs_consumer(bureau):
    warnings = {}
    typeXML = "MB"
    tradelines = bureau.accounts.tradeline.rows

    tradelines = del_gold_loan_agri_loans_ownership("MB", tradelines)

    sourcing_dates = bureau.sourcing_date
    current_date = (
            datetime.strptime(sourcing_dates, '%d-%m-%Y') - dateutil.relativedelta.relativedelta(
        months=0)).date()

    disbursed_dates = [x['disbursed_dt'] for x in tradelines]

    inquiry_dates = [x['inquiry_dates'] for x in bureau.accounts.li if x["inquiry_type"] in ["Personal Loan", "Business Loan General", "Credit Card"]]

    no_hit = check_no_hit_with_months(sourcing_dates, disbursed_dates, months=6)

    nbr_unsecured_enquiry_1_mth = int(inq_date_fun_hfs(inquiry_dates, current_date, 30))
    nbr_unsecured_enquiry_3_mth = int(inq_date_fun_hfs(inquiry_dates, current_date, 91))

    if nbr_unsecured_enquiry_3_mth <= 6:
        pol_no_6_unsecured_enq_3mth = True
    else:
        pol_no_6_unsecured_enq_3mth = False
    try:
        bureau_name_list = list(filter(None, [x['bureau_name'] for x in tradelines]))
    except:
        bureau_name_list = []
    try:
        bureau_name = max(set(bureau_name_list), key=bureau_name_list.count)
    except:
        bureau_name = None
        warnings["BureauName"] = "No bureau name in xml"
    try:
        bureau_score = int(bureau.score)
    except:
        bureau_score = 0

    if bureau_score not in [None] and bureau_score >= 700:
        bureau_status = True
    else:
        bureau_status = False

    num_of_tradelines = bureau.accounts.tradeline.num_of_tradeline
    if int(num_of_tradelines) > 0 and not no_hit:
        type_of_xml = 'MB'
        if typeXML in ["MB", "MULTIBUREAU", "MULTI_BUREAU"]:
            tradelines = dpd_cleaner1(tradelines)
            type_of_xml = "MB"
        elif typeXML in ['EXPERIAN', 'EXP']:
            type_of_xml = "EXP"
            tradelines = dpd_cleaner_exp(tradelines)

        for row in tradelines:
            row["source_date"] = sourcing_dates
            row["current_date"] = current_date
            row["ownership_tag"] = ownership_tag_fun(row)
            mob_source = mob_source_date(row)
            row['mob_source_date'] = mob_source
            row['mob'] = mob_source
            row["live_account"] = live_account_cleaner(row["current_date"], row['close_dt'])
            row["pol_only_gold_loan"] = pol_only_gold_loan_fun(row)
            row["pol_bl_loan"] = pol_bl_loan_fun(row)
            row["pol_only_guarantor"] = pol_only_guarantor_fun(row)
            row['pol_only_CD'] = pol_only_cd_fun(row)
            row['credit_card_limit'] = credit_card_limit(row)
            row['credit_limit'] = credit_limit(row)
            # HFS functions
            row["pol_no_derog_ever"] = pol_no_derog_ever_fun_hfs(row, type_of_xml)
            row["no_derog_36mths"] = no_derog_36mths_fun_hfs(row, type_of_xml)
            row['pol_0_plus_3mth'] = pol_dpd_plus_mths_disbursed_dt_fun_hfs(row, sourcing_dates, 0, 3, type_of_xml)
            row["pol_0_plus_3mth_req_date"] = pol_dpd_plus_mths_fun_hfs(row, current_date, 0, 3, type_of_xml)
            row["pol_30_plus_3mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, current_date, 30, 3, type_of_xml)
            row["pol_90_plus_36mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, current_date, 90, 36, type_of_xml)
            row["pol_60_plus_24mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, current_date, 60, 24, type_of_xml)
            row["pol_30_plus_12mth"] = pol_dpd_eq_plus_mths_fun_hfs(row, current_date, 30, 12, type_of_xml)
            row["pol_0_plus_6mth"] = pol_0_plus_current_fun_hfs(row, current_date, 0, 6, type_of_xml)
            row["pol_only_gold_loan"] = pol_only_gold_loan_func_hfs(row, type_of_xml)
            row["pol_bl_loan"] = pol_bl_loan_func_hfs(row, type_of_xml)

        # tradelines = del_pl_tradeline(tradelines)

        live_account_list, pol_only_gold_loan_list, pol_bl_loan_list, pol_only_guarantor_list, \
            no_derog_36mths_list, pol_no_derog_ever_list, pol_60_plus_24mth_list, overdue_list, \
            current_balance_list, disbursed_amount_list, unsec_amt_overdue_list, \
            pol_0_plus_3mth_list, mob_source_date_list, bl_bureau_vintage_list, \
            pol_30_plus_3mth_list, unsec_perc_overdue_amt_list, unsec_perc_disbursed_amt_list, \
            unsec_perc_current_bal_list, pol_0_plus_in_6m_curr_balance_list, \
            pol_0_plus_in_6m_disbursed_amt_list, pol_30plus_in_6m_current_bal_list, \
            pol_30plus_in_6m_disbursed_amt_list, pol_90_plus_36mth_list, \
            pol_30_plus_12mth_list, pol_0_plus_6mth_list, pol_0_plus_3mth_req_date_list = calculated_variables(tradelines)

        total_live_accounts = sum(live_account_list)

        total_overdue = 0 if not overdue_list else int(sum(overdue_list))
        pol_overdue = False if total_overdue > 0 else True
        try:
            os_to_loan_amount = sum(current_balance_list) / sum(disbursed_amount_list)
        except:
            os_to_loan_amount = 0

        unsecured_amt_overdue = sum(unsec_amt_overdue_list)

        ratio_num_trdlns_0plus_in_3m = pol_0_plus_3mth_list.count(1) / num_of_tradelines

        bureau_vintage = int(max(mob_source_date_list)) / 12

        min_mob = min(mob_source_date_list)

        last_max_loan_amount = last_max_loan_amount_fun_hfs(tradelines, min_mob)

        max_loan_amount = max([float(str(x['disbursed_amt']).replace(',', '')) for x in tradelines])

        bl_bureau_vintage = max(bl_bureau_vintage_list)

        try:
            unsec_perc_loan_amt = sum(unsec_perc_overdue_amt_list) / sum(unsec_perc_disbursed_amt_list)
        except:
            unsec_perc_loan_amt = 0

        last_max_unsec_loan_amount = last_max_unsec_loan_amount_fun_hfs(tradelines, min_mob)

        last_max_bl_loan_amount = last_max_bl_loan_amount_fun_hfs(tradelines, min_mob)

        pol_0_plus_in_6m_curr_balance_sum = sum(pol_0_plus_in_6m_curr_balance_list)
        pol_0_plus_in_6m_disbursed_amt_sum = sum(pol_0_plus_in_6m_disbursed_amt_list)
        try:
            zero_plus_in_6m_os_to_loan_amount = float(pol_0_plus_in_6m_curr_balance_sum/pol_0_plus_in_6m_disbursed_amt_sum)
        except:
            zero_plus_in_6m_os_to_loan_amount = 0

        try:
            unsec_perc_os_amt = sum(unsec_perc_current_bal_list) / sum(unsec_perc_disbursed_amt_list)
        except:
            unsec_perc_os_amt = 0

        num_trdlns_30plus_in_3m = pol_30_plus_3mth_list.count(1)

        num_trdlns_0plus_in_3m = pol_0_plus_3mth_req_date_list.count(1)

        try:
            num_30plus_in_6m_os_to_loan_amount = sum(pol_30plus_in_6m_current_bal_list) / sum(pol_30plus_in_6m_disbursed_amt_list)
        except:
            num_30plus_in_6m_os_to_loan_amount = 0

        pol_only_gold_loan = all(pol_only_gold_loan_list)
        pol_bl_loan = any(pol_bl_loan_list)

        pol_no_derog_36mths = False if int(sum(no_derog_36mths_list)) > 0 else True

        pol_no_90_plus_36mths = False if int(sum(pol_90_plus_36mth_list)) > 0 else True

        pol_no_60_plus_24mths = False if int(sum(pol_60_plus_24mth_list)) > 0 else True

        pol_no_30_plus_12mths = False if int(sum(pol_30_plus_12mth_list)) > 0 else True
        pol_no_0_plus_6mth = False if int(sum(pol_0_plus_6mth_list)) > 0 else True

        # pol_only_gold_loan is False and \

        if pol_no_derog_36mths is True and \
                pol_no_90_plus_36mths is True and \
                pol_no_60_plus_24mths is True and \
                pol_no_30_plus_12mths is True and \
                pol_no_0_plus_6mth is True:
            if pol_no_6_unsecured_enq_3mth is False:
                pol_decision_status = "RTC"
            else:
                pol_decision_status = "Accept"
        else:
            pol_decision_status = "Decline"

        if pol_decision_status in ["Decline"]:
            rejection_reason = rejection_reasons(bureau_status, bureau_score, pol_no_derog_36mths, pol_no_90_plus_36mths,
                                                  pol_no_60_plus_24mths, pol_no_30_plus_12mths, pol_no_0_plus_6mth,
                                                  pol_no_6_unsecured_enq_3mth)
        else:
            rejection_reason = []

        dict_resp = {'bureau_type': "Consumer",
                     "AmtOverdue": total_overdue,
                     "os_to_loan_amount": float("{0:.4f}".format(os_to_loan_amount)),
                     "unsec_AmtOverdue": unsecured_amt_overdue,
                     "ratio_num_trdlns_0plus_in_3m": float("{0:.4f}".format(ratio_num_trdlns_0plus_in_3m)),
                     "bureau_vintage": float("{0:.4f}".format(bureau_vintage)),
                     "max_loan_amount": max_loan_amount,
                     "last_max_loan_amount": last_max_loan_amount,
                     "bl_bureau_vintage": bl_bureau_vintage,
                     "unsec_perc_loan_amt": unsec_perc_loan_amt,
                     "last_max_unsec_loan_amount": last_max_unsec_loan_amount,
                     "last_max_bl_loan_amount": last_max_bl_loan_amount,
                     "0plus_in_6m_CurrBalance": pol_0_plus_in_6m_curr_balance_sum,
                     "0plus_in_6m_os_to_loan_amount": zero_plus_in_6m_os_to_loan_amount,
                     "unsec_perc_os_amt": unsec_perc_os_amt,
                     "num_trdlns_30plus_in_3m": num_trdlns_30plus_in_3m,
                     "num_trdlns_0plus_in_3m": num_trdlns_0plus_in_3m,
                     "30plus_in_6m_os_to_loan_amount": num_30plus_in_6m_os_to_loan_amount,
                     "nbr_enq_bl_1m": nbr_unsecured_enquiry_1_mth,
                     # ------------Only Response variables --------
                     "total_live_accounts": total_live_accounts,
                     "nbr_unsecured_enquiry_3_mth": nbr_unsecured_enquiry_3_mth,
                     "no_hit": no_hit,
                     "bureau_name": bureau_name,
                     "bureau_score": bureau_score,
                     "bureau_status": bureau_status,
                     "pol_overdue": pol_overdue,
                     "pol_no_derog_36mths": pol_no_derog_36mths,
                     "pol_no_90_plus_36mths": pol_no_90_plus_36mths,
                     "pol_no_60_plus_24mths": pol_no_60_plus_24mths,
                     "pol_no_30_plus_12mths": pol_no_30_plus_12mths,
                     "pol_no_0_plus_6mth": pol_no_0_plus_6mth,
                     "pol_only_gold_loan": pol_only_gold_loan,
                     "pol_any_bl_loan": pol_bl_loan,
                     "pol_decision_status": pol_decision_status,
                     "rejection_reason": rejection_reason
                     }

        return dict_resp
    else:
        if num_of_tradelines > 0:
            if no_hit:
                rejection_reason = ['no_hit is True (NTC)']
            else:
                rejection_reason = ["no_hit is False"]
        else:
            rejection_reason = ['No Tradelines in Bureau']

        return {'bureau_type': "Consumer",
                "AmtOverdue": None,
                "os_to_loan_amount": None,
                "unsec_AmtOverdue": None,
                "ratio_num_trdlns_0plus_in_3m": None,
                "bureau_vintage": None,
                "max_loan_amount": None,
                "last_max_loan_amount": None,
                "bl_bureau_vintage": None,
                "unsec_perc_loan_amt": None,
                "last_max_unsec_loan_amount": None,
                "last_max_bl_loan_amount": None,
                "0plus_in_6m_CurrBalance": None,
                "0plus_in_6m_os_to_loan_amount": None,
                "unsec_perc_os_amt": None,
                "num_trdlns_30plus_in_3m": None,
                "num_trdlns_0plus_in_3m": None,
                "30plus_in_6m_os_to_loan_amount": None,
                "nbr_enq_bl_1m": None,
                # ------------Only Response variables --------
                "total_live_accounts": None,
                "nbr_unsecured_enquiry_3_mth": None,
                "no_hit": no_hit,
                "hit_status": no_hit,
                "bureau_name": bureau_name,
                "bureau_score": bureau_score,
                "bureau_status": bureau_status,
                "pol_overdue": None,
                "pol_no_derog_36mths": None,
                "pol_no_90_plus_36mths": None,
                "pol_no_60_plus_24mths": None,
                "pol_no_30_plus_12mths": None,
                "pol_no_0_plus_6mth": None,
                "pol_only_gold_loan": None,
                "pol_any_bl_loan": None,
                "pol_decision_status": "Decline",
                "rejection_reason": rejection_reason
                }